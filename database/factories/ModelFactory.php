<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */



// $factory->define(App\Room::class, function(Faker\Generator $faker){
// 	 return[
// 	 	'isAvailable'=>rand(0,1),
// 	 	'roomtype_id'=>$faker->numberBetween($min=1, $max=3),
// 	 	'additionalNotes'=>$faker->paragraph($nbSentences=2, $variableNbSentences=true)
// 	 ];
// });

// $factory->define(App\Roomtype::class, function(Faker\Generator $faker){
// 	return[
// 		'roomtype_name'=>$faker->word($nb=rand(1,3), $asText=true),
// 		'roomtype_desc'=>$faker->paragraph($nbSenteces=2, $variableNbSentences=true),
// 		'rate'=>$faker->randomFloat($nbMaxDecimals=2, $min=1000, $max=5000),
// 		'capacity'=>$faker->numberBetween($min=1, $max=15),

// 	];
// });

// $factory->define(App\Reservation::class, function(Faker\Generator $faker){
// 	return[
// 		'startDate'=>$faker->dateTimeThisMonth($max='now'),
// 		'endDate'=>$faker->dateTimeThisMonth($max='now'),
// 		'user_id'=>1,
// 		'amount'=>5000,
// 		'room_id'=>3
// 	];
// });

// $factory->define(App\Photo::class, function(Faker\Generator $faker){
// 	return[
// 		'path'=>"https://pixabay.com/get/e832b10920f4003ed1534705fb0938c9bd22ffd41db6174694f5c47fa3/room-1706801_1920.jpg",
// 		'room_id'=>$faker->numberBetween($min=1, $max=20)

// 	];

// });