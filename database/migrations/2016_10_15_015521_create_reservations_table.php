<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('start_date');
            $table->date('end_date');
            $table->date('cutoff_date');
            $table->integer('stay_length');
            $table->integer('user_id')->nullable();
            $table->integer('adults')->unsigned();
            $table->integer('children')->unsigned()->default(0);
            $table->decimal('amount');
            $table->integer('room_id');
            $table->integer('charge_id');
            $table->string('firstname');
            $table->string('middlename')->nullable();
            $table->string('lastname');
            $table->string('confirmation_code')->nullable()->unique();
            $table->tinyInteger('status')->default(0)->unsigned();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
