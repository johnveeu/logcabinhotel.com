<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ammenities', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('ammenity_name');
            $table->string('ammenity_desc');
            $table->string('ammenity_code')->unique();
            $table->integer('max');
            $table->double('price', 5,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ammenities');
    }
}
