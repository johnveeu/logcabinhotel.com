<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
class ReservationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservations')->insert([
        	'start_date' => Carbon::createFromDate('2016','11','27'),
        	'end_date' => Carbon::createFromDate('2016','11','28'),
        	'amount' => 5000,
        	'room_id' => 1,
        	'first_name' => 'Juan',
        	'middle_name' => 'Este',
        	'last_name' => 'Tamad',
        	'email' => 'example@email.com',
        	'paypal_email' => 'examplepp@gmail.com'
        ]);
        DB::table('reservations')->insert([
        	'start_date' => Carbon::createFromDate('2016','11','27'),
        	'end_date' => Carbon::createFromDate('2016','11','28'),
        	'amount' => 5000,
        	'room_id' => 2,
        	'first_name' => 'Juan',
        	'middle_name' => 'Este',
        	'last_name' => 'Tamad',
        	'email' => 'example@email.com',
        	'paypal_email' => 'examplepp@gmail.com'
        ]);
        DB::table('reservations')->insert([
        	'start_date' => Carbon::createFromDate('2016','11','28'),
        	'end_date' => Carbon::createFromDate('2016','11','29'),
        	'amount' => 5000,
        	'room_id' => 3,
        	'first_name' => 'Qwe',
        	'middle_name' => 'Qqwe',
        	'last_name' => 'Qwe',
        	'email' => 'example@email.com',
        	'paypal_email' => 'examplepp@gmail.com'
        ]);
        DB::table('reservations')->insert([
        	'start_date' => Carbon::createFromDate('2016','12','1'),
        	'end_date' => Carbon::createFromDate('2016','12','2'),
        	'amount' => 5000,
        	'room_id' => 4,
        	'first_name' => 'Bla',
        	'middle_name' => 'Bla',
        	'last_name' => 'Bla',
        	'email' => 'example@email.com',
        	'paypal_email' => 'examplepp@gmail.com'
        ]);
        DB::table('reservations')->insert([
        	'start_date' => Carbon::createFromDate('2016','12','1'),
        	'end_date' => Carbon::createFromDate('2016','12','2'),
        	'amount' => 5000,
        	'room_id' => 5,
        	'first_name' => 'Juan',
        	'middle_name' => 'Este',
        	'last_name' => 'Tamad',
        	'email' => 'example@email.com',
        	'paypal_email' => 'examplepp@gmail.com'
        ]);
        DB::table('reservations')->insert([
        	'start_date' => Carbon::createFromDate('2016','11','7'),
        	'end_date' => Carbon::createFromDate('2016','12','7'),
        	'amount' => 5000,
        	'room_id' => 6,
        	'first_name' => 'Juan',
        	'middle_name' => 'Este',
        	'last_name' => 'Tamad',
        	'email' => 'example@email.com',
        	'paypal_email' => 'examplepp@gmail.com'
        ]);
    }
}
