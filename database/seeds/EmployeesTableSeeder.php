<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('employees')->insert([
            'username' => 'employee',
            'password' => bcrypt('password')
        ]);

        DB::table('users')->insert([
            'firstname' => 'Employee',
            'middlename' => 'E',
            'lastname' => 'Safari',
            'userable_id' => 1,
            'userable_type' => 'App\Employee'
        ]);

    }
}
