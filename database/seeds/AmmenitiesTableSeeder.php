<?php

use Illuminate\Database\Seeder;

class AmmenitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ammenities')->insert([
        	'ammenity_name' => 'Bed & Breakfast',
        	'ammenity_desc' => 'Additional bed for one with free breakfast',
        	'ammenity_code' => 'BNB',
        	'max' => 5,
        	'price' => 750.00
       	]);
    }
}
