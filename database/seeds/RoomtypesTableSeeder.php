<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class RoomtypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

    	$path = 'http://logcabinhotel.com/img/rooms/';

        DB::table('roomtypes')->insert([
        	'roomtype_name' => 'Suite',
        	'roomtype_desc' => 'A perfect room for families.',
            'ammenities' => 'TV, Telephone, Air Conditioning, Bathrobe, Desk, Free toiletries, Toilet, Bathroom, Slippers, Satellite Channels, Cable Channels, Bath or Shower, Flat screen TV, Electric kettle, Wardrobe/Closet, Bidet, Hypoallergenic, Towels, Linen, Clothes rack',
            'capacity' => 5,
  			'thumbnail_path' => $path.'TFS/GOPR0255-min.JPG',
  			'created_at' => Carbon::now()
        ]);

        DB::table('roomtypes')->insert([
            'roomtype_name' => 'Deluxe',
            'roomtype_desc' => $faker->text,
            'ammenities' => 'TV, Telephone, Air Conditioning, Bathrobe, Desk, Free toiletries, Toilet, Bathroom, Slippers, Satellite Channels, Cable Channels, Bath or Shower, Flat screen TV, Electric kettle, Wardrobe/Closet, Bidet, Hypoallergenic, Towels, Linen, Clothes rack',
            'capacity' => 2,
            'thumbnail_path' => $path.'9/GOPR0329-min.JPG',
            'created_at' => Carbon::now()
        ]);


        DB::table('roomtypes')->insert([
        	'roomtype_name' => 'Standard',
        	'roomtype_desc' => $faker->text,
            'ammenities' => 'TV, Telephone, Air Conditioning, Bathrobe, Desk, Free toiletries, Toilet, Bathroom, Slippers, Satellite Channels, Cable Channels, Bath or Shower, Flat screen TV, Electric kettle, Wardrobe/Closet, Bidet, Hypoallergenic, Towels, Linen, Clothes rack',
            'capacity' => 2,
			'thumbnail_path' => $path.'27/GOPR0229-min.JPG',
			'created_at' => Carbon::now()
        ]);

        DB::table('roomtypes')->insert([
            'roomtype_name' => 'Premier',
            'roomtype_desc' => $faker->text,
            'ammenities' => 'TV, Telephone, Air Conditioning, Bathrobe, Desk, Free toiletries, Toilet, Bathroom, Slippers, Satellite Channels, Cable Channels, Bath or Shower, Flat screen TV, Electric kettle, Wardrobe/Closet, Bidet, Hypoallergenic, Towels, Linen, Clothes rack',
            'capacity' => 4,
            'thumbnail_path' => $path.'2/GOPR0324-min.JPG',
            'created_at' => Carbon::now()
        ]);


    }
}
