<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'email' => 'customer@example.com',
            'paypal_email' => 'customer@paypal.com',
            'password' => bcrypt('password')
        ]);

        DB::table('users')->insert([
            'firstname' => 'Customer',
            'middlename' => 'C',
            'lastname' => 'Safari',
            'userable_id' => 1,
            'userable_type' => 'App\Customer'
        ]);

    }
}
