<?php

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
	DB::table('photos')->insert(['room_id'=>1,
		'path' => 'http://logcabinhotel.com/img/rooms/tbs/GOPRO375-min.jpg, http://logcabinhotel.com/img/rooms/tbs/GOPRO376-min.jpg, http://logcabinhotel.com/img/rooms/tbs/GOPRO379-min.jpg, http://logcabinhotel.com/img/rooms/tbs/GOPRO427-min.jpg, http://logcabinhotel.com/img/rooms/tbs/GOPRO429-min.jpg,' 
		]);

	DB::table('photos')->insert(['room_id'=>1,
		'path' => 'http://logcabinhotel.com/img/rooms/tfs/GOPRO255-min.jpg, http://logcabinhotel.com/img/rooms/tfs/GOPRO263-min.jpg, http://logcabinhotel.com/img/rooms/tfs/GOPRO265-min.jpg, http://logcabinhotel.com/img/rooms/tfs/GOPRO267-min.jpg, http://logcabinhotel.com/img/rooms/tfs/GOPRO487-min.jpg, http://logcabinhotel.com/img/rooms/tfs/GOPRO489-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>2,
		'path' => 'http://logcabinhotel.com/img/rooms/7/GOPRO359-min.jpg, http://logcabinhotel.com/img/rooms/7/GOPRO362-min.jpg, http://logcabinhotel.com/img/rooms/7/GOPRO364-min.jpg, http://logcabinhotel.com/img/rooms/7/GOPRO366-min.jpg' 
		]);
	
	DB::table('photos')->insert(['room_id'=>2,
		'path' => 'http://logcabinhotel.com/img/rooms/8/GOPRO337-min.jpg, http://logcabinhotel.com/img/rooms/8/GOPRO339-min.jpg, http://logcabinhotel.com/img/rooms/8/GOPRO341-min.jpg, http://logcabinhotel.com/img/rooms/8/GOPRO342-min.jpg'
		]);

	DB::table('photos')->insert(['room_id'=>2,
		'path' => 'http://logcabinhotel.com/img/rooms/9/GOPRO327-min.jpg, http://logcabinhotel.com/img/rooms/9/GOPRO329-min.jpg, http://logcabinhotel.com/img/rooms/9/GOPRO330-min.jpg, http://logcabinhotel.com/img/rooms/9/GOPRO333-min.jpg, http://logcabinhotel.com/img/rooms/9/GOPRO336-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>2,
		'path' => 'http://logcabinhotel.com/img/rooms/22/GOPRO250-min.jpg, http://logcabinhotel.com/img/rooms/22/GOPRO252-min.jpg, http://logcabinhotel.com/img/rooms/22/GOPRO253-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>2,
		'path' => 'http://logcabinhotel.com/img/rooms/25/GOPRO240-min.jpg, http://logcabinhotel.com/img/rooms/25/GOPRO241-min.jpg, http://logcabinhotel.com/img/rooms/25/GOPRO242-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>2,
		'path' => 'http://logcabinhotel.com/img/rooms/26/GOPRO230-min.jpg, http://logcabinhotel.com/img/rooms/26/GOPRO232-min.jpg, http://logcabinhotel.com/img/rooms/26/GOPRO235-min.jpg, http://logcabinhotel.com/img/rooms/26/GOPRO237-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>3,
		'path' => 'http://logcabinhotel.com/img/rooms/31/GOPRO268-min.jpg, http://logcabinhotel.com/img/rooms/31/GOPRO271-min.jpg, http://logcabinhotel.com/img/rooms/31/GOPRO272-min.jpg, http://logcabinhotel.com/img/rooms/31/GOPRO273-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>3,
		'path' => 'http://logcabinhotel.com/img/rooms/32/GOPRO275-min.jpg, http://logcabinhotel.com/img/rooms/32/GOPRO281-min.jpg, http://logcabinhotel.com/img/rooms/32/GOPRO282-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>3,
		'path' => 'http://logcabinhotel.com/img/rooms/33/GOPRO283-min.jpg, http://logcabinhotel.com/img/rooms/33/GOPRO287-min.jpg, http://logcabinhotel.com/img/rooms/33/GOPRO289-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>3,
		'path' => 'http://logcabinhotel.com/img/rooms/35/GOPRO291-min.jpg, http://logcabinhotel.com/img/rooms/35/GOPRO294-min.jpg, http://logcabinhotel.com/img/rooms/35/GOPRO297-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>4,
		'path' => 'http://logcabinhotel.com/img/rooms/2/GOPRO319-min.jpg, http://logcabinhotel.com/img/rooms/2/GOPRO320-min.jpg, http://logcabinhotel.com/img/rooms/2/GOPRO322-min.jpg, http://logcabinhotel.com/img/rooms/2/GOPRO324-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>4,
		'path' => 'http://logcabinhotel.com/img/rooms/3/GOPRO319-min.jpg, http://logcabinhotel.com/img/rooms/3/GOPRO320-min.jpg, http://logcabinhotel.com/img/rooms/3/GOPRO322-min.jpg, http://logcabinhotel.com/img/rooms/3/GOPRO324-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>4,
		'path' => 'http://logcabinhotel.com/img/rooms/5/GOPRO319-min.jpg, http://logcabinhotel.com/img/rooms/5/GOPRO320-min.jpg, http://logcabinhotel.com/img/rooms/5/GOPRO322-min.jpg, http://logcabinhotel.com/img/rooms/5/GOPRO324-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>4,
		'path' => 'http://logcabinhotel.com/img/rooms/6/GOPRO367-min.jpg, http://logcabinhotel.com/img/rooms/6/GOPRO369-min.jpg, http://logcabinhotel.com/img/rooms/6/GOPRO370-min.jpg' 
		]);

	DB::table('photos')->insert(['room_id'=>4,
		'path' => 'http://logcabinhotel.com/img/rooms/27/GOPRO223-min.jpg, http://logcabinhotel.com/img/rooms/27/GOPRO226-min.jpg, http://logcabinhotel.com/img/rooms/27/GOPRO228-min.jpg, http://logcabinhotel.com/img/rooms/27/GOPRO229-min.jpg' 
		]);




    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    }
}
