<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('admins')->insert([
            'username' => 'admin@example.com',
            'password' => bcrypt('password')
        ]);

        DB::table('users')->insert([
            'firstname' => 'Admin',
            'middlename' => 'A',
            'lastname' => 'Safari',
            'userable_id' => 1,
            'userable_type' => 'App\Admin'
        ]);

        DB::table('admins')->insert([
            'username' => 'admin',
            'password' => bcrypt('password')
        ]);

        DB::table('users')->insert([
            'firstname' => 'Blabla',
            'middlename' => 'A',
            'lastname' => 'Safari',
            'userable_id' => 2,
            'userable_type' => 'App\Admin'
        ]);
    }
}
