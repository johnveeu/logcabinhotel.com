<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('rooms')->insert([
            'roomtype_id' => 1,
            'room_name' => 'Botanical Suite',
            'capacity' => 5,
            'offseason_rate' => 17800,
            'midseason_rate' => 7740,
            'peakseason_rate' => 8900
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 1,
            'room_name' => 'Fireplace Suite',
            'capacity' => 4,
            'offseason_rate' => 13800,
            'midseason_rate' => 6000,
            'peakseason_rate' => 6900
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 2,
            'room_name' => 'M7',
            'capacity' => 2,
            'offseason_rate' => 7400,
            'midseason_rate' => 3218,
            'peakseason_rate' => 3700
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 2,
            'room_name' => 'M8',
            'capacity' => 2,
            'offseason_rate' => 8400,
            'midseason_rate' => 3653,
            'peakseason_rate' => 4200
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 2,
            'room_name' => 'M9',
            'capacity' => 2,
            'offseason_rate' => 7800,
            'midseason_rate' => 3392,
            'peakseason_rate' => 3900
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 2,
            'room_name' => 'N22',
            'capacity' => 2,
            'offseason_rate' => 6600,
            'midseason_rate' => 3044,
            'peakseason_rate' => 3500
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 2,
            'room_name' => 'N23',
            'capacity' => 2,
            'offseason_rate' => 7000,
            'midseason_rate' => 3044,
            'peakseason_rate' => 3500
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 2,
            'room_name' => 'N25',
            'capacity' => 2,
            'offseason_rate' => 7000,
            'midseason_rate' => 2870,
            'peakseason_rate' => 3300
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 2,
            'room_name' => 'N26',
            'capacity' => 2,
            'offseason_rate' => 6600,
            'midseason_rate' => 3914,
            'peakseason_rate' => 4500
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 3,
            'room_name' => 'P31',
            'capacity' => 2,
            'offseason_rate' => 4200,
            'midseason_rate' => 2100,
            'peakseason_rate' => 1827
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 3,
            'room_name' => 'P32',
            'capacity' => 2,
            'offseason_rate' => 5000,
            'midseason_rate' => 2500,
            'peakseason_rate' => 2174
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 3,
            'room_name' => 'P33',
            'capacity' => 2,
            'offseason_rate' => 3800,
            'midseason_rate' => 1653,
            'peakseason_rate' => 1900
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 3,
            'room_name' => 'P35',
            'capacity' => 4,
            'offseason_rate' => 5600,
            'midseason_rate' => 2435,
            'peakseason_rate' => 2800
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 4,
            'room_name' => 'M2',
            'capacity' => 4 ,
            'offseason_rate' => 9800,
            'midseason_rate' => 4261,
            'peakseason_rate' => 4900
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 4,
            'room_name' => 'M3',
            'capacity' => 4,
            'offseason_rate' => 10400,
            'midseason_rate' => 5000,
            'peakseason_rate' => 4522
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 4,
            'room_name' => 'M5',
            'capacity' => 4,
            'offseason_rate' => 11000,
            'midseason_rate' => 4783,
            'peakseason_rate' => 5500
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 4,
            'room_name' => 'M6',
            'capacity' => 4,
            'offseason_rate' => 12400,
            'midseason_rate' => 5000,
            'peakseason_rate' => 5392
        ]);

        DB::table('rooms')->insert([
            'roomtype_id' => 4,
            'room_name' => 'N27',
            'capacity' => 3,
            'offseason_rate' => 9000,
            'midseason_rate' => 2870,
            'peakseason_rate' => 3300
        ]);
    }
}
