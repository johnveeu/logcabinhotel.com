<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Reservation;

use App\Room;

use App\Roomtype;

use App\User;

use App\Charge;

use App\Ammenity;

use Carbon\Carbon;

use Stripe\Stripe;

use Stripe\Refund;

use Illuminate\Support\Facades\Auth;

class ReservationDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Reservation::all();
        return view('dashboard.reservation.index')->with('reservations', $reservations);    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reservation = Reservation::find($id);

        return view('dashboard.reservation.show')->with('reservation', $reservation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $reservation = Reservation::find($id);
        $roomtypes = Roomtype::all();
        $customers = User::where('userable_type', 'App\Customer');
        $ammenities = Ammenity::all();

        return view('dashboard.reservation.edit')->with('reservation', $reservation)->with('roomtypes', $roomtypes)->with('customers', $customers)->with('ammenities', $ammenities);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ammenitiesId = array();
        $ammenityValues = array();
        $ammenities = Ammenity::all();
        foreach ($ammenities as $key => $ammenity) {
            if($request->has($ammenity->ammenity_code)){
                $ammenitiesId[] = $ammenity->id;
                $ammenityValues[]['qty'] =  $request->get($ammenity->ammenity_code);
            }
        }

        $syncData = array_combine($ammenitiesId, $ammenityValues);

        $reservation = Reservation::find($id);

        $reservation->start_date = $request->check_in;
        $reservation->end_date = $request->check_out;
        $reservation->cutoff_date = $request->cutoff;
        $reservation->stay_length = $request->stay_length;
        $reservation->adults = (int) $request->adults;
        $reservation->children = (int) $request->children;
        $reservation->amount = $request->total;
        $reservation->room_id = $request->room_id;
        $reservation->firstname = $request->firstname;
        $reservation->lastname = $request->lastname;

        $charge = Charge::find($reservation->charge->id);
        $charge->amount = $request->charge*100;

        $reservation->save();
        $charge->save();

        $reservation->ammenities()->sync($syncData);

        if(Auth::user()->user->userable_type=="App\Admin"){
            return redirect('/admin/dashboard/reservations/'.$id)->with('success', 'Reservation updated succesfully');
        }

        return redirect('/employee/dashboard/reservations/'.$id)->with('success', 'Reservation updated succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservation = Reservation::find($id);

        Stripe::setApiKey("sk_test_Jsg0MVddWCbfSf8r6MZfa7Tk");

        $re = Refund::create(array(
          "charge" => $reservation->charge->charge_code,
        ));

        $reservation->delete();

        if(Auth::user()->user->userable_type=="App\Admin"){
            return redirect('/admin/dashboard/reservations')->with('success', 'Reservation deleted succesfully');
        }

        return redirect('/employee/dashboard/reservations')->with('success', 'Reservation deleted succesfully');
    }

    public function checkRoomAvailability(Request $request){
        $checkIn = $request->checkIn;
        $checkOut = $request->checkOut; 
        $tempRooms = Room::with('roomtype')->whereNotIn('id', function($query) use($checkIn, $checkOut){
            $query->select('room_id')
                ->from(with(new Reservation)->getTable())
                ->where([['start_date', '<',$checkOut], ['end_date', '>', $checkIn]])
                ->whereOr([['start_date', '<', $checkIn],['end_date', '>', $checkOut]]);
        })->get();

        foreach($tempRooms as $room){
            if($room->id == $request->id){
                return("true");
            }
        }

        return "Room is not available on these dates!";
    }

    public function changeRoomRates(Request $request){
        $room = Room::with('roomtype')->find($request->id);

        return $room;
    }

    public function getSeason(Request $request){
        $season = 3;
        $date = Carbon::parse($request->date);
        if((($date->month==1&&$date->day<=1)||($date->month==12&&$date->day>=16))||
            (($date->month==2))){
            $season = 1;
        }elseif((($date->month==6&&$date->day>=16)||($date->month==10&&$date->day<14))||(($date->month>6&&$date->month<10))){
            $season = 2;
        }
        return $season;
    }
}
