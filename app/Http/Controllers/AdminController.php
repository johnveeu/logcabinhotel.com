<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Admin;

use App\User;

use App\Http\Requests\AdminNewRequest;

use App\Http\Requests\AdminUpdateRequest;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.users.admin-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminNewRequest $request)
    {
        $admin = new Admin;

        $admin->username = $request->username;
        $admin->password = bcrypt($request->password);

        $admin->save();

        $user = new User;
        $user->firstname = $request->firstname;
        $user->middlename = '';
        $user->lastname = $request->lastname;
        $user->userable_id = $admin->id;
        $user->userable_type = 'App\Admin';

        $user->save();

        return redirect(url('admin/dashboard/users'))->with('success', 'Administrator succesfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $admin = Admin::find($id);
        return view('dashboard.users.admin-update')->with('admin', $admin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUpdateRequest $request, $id)
    {
        $admin = Admin::find($id);

        $admin->username = $request->username;

        $user = User::find($admin->id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;

        $admin->save();
        $user->save();

        return redirect(url('admin/dashboard/users'))->with('success', 'Administrator succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $admin = Admin::find($id);

        $admin->user->delete();
        $admin->delete();

        return redirect(url('admin/dashboard/users'))->with('success', 'Administrator succesfully deleted');
    }
}
