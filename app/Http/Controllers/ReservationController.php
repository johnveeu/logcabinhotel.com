<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Reservation;

use Illuminate\View\View;

use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;

use App\Room;

use Stripe\Stripe;

use Stripe\Refund;

use App\Mail\UpdateReservationVoucher;

use App\Charge;
class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('customer-reservation.reservation-step-1');   
    }


    public function update(Request $request){
        $res = Reservation::find($request->reservation_id);

        $date = Carbon::parse($request->checkIn);

        $cutoff_date = $date->subDays(7);

        $res->start_date = $request->checkIn;
        $res->end_date = $request->checkOut;
        $res->cutoff_date = $cutoff_date;

        $res->save();

        Mail::to(
            ['to' => [
                'address' => $res->user->userable->email,
                'email' => $res->user->userable->email,
                'name' => $res->firstname.' '.$res->lastname,
            ]]
            )->send(new UpdateReservationVoucher($res));

        return view('ammend-reservation.show')->with('reservation', $res)->with('edit_success', 'This reservation has been moved. The updated details is shown below. Your reservation voucher has also been update. Please check your e-mail.');
    }

    public function getReservationStep(Request $request, $step){
        if($step==1){
            session(['step' => 1]);
            session(['reservation_status' => 0]);
        }

        if($step==5){
            return view('customer-reservation.reservation-step-'.$step);
        }

        if(session('step')==1){
            return view('customer-reservation.reservation-step-1');
        }

        if($request->session()->get('step')!=$step){
            return redirect()->action('ReservationController@getReservationStep', ['step' => session('step')]);
        }        

        return view('customer-reservation.reservation-step-'.$step);
    }

    public function showCheckForm(){
        return view('ammend-reservation.check');
    }

    public function verifyInfo(Request $request){
        $reservation = Reservation::where('confirmation_code', $request->conf_code)->first();

        if(!$reservation){
            return redirect()->back()->with('error', 'No reservation code found.');
        }

        if($reservation->user->userable->email != $request->email){
            return redirect()->back()->with('error', 'It seems you entered the wrong e-mail address.');
        }

        return view('ammend-reservation.show')->with('reservation', $reservation);
    }

    public function cancelReservation(Request $request){
        Stripe::setApiKey("sk_test_Jsg0MVddWCbfSf8r6MZfa7Tk");

        $reservation = Reservation::find($request->reservation_id);

        if(!Carbon::now()->lte(Carbon::parse($reservation->cutoff_date))){
            $re = Refund::create(array(
              "charge" => $request->charge_code,
            ));
        }

        $reservation->status = 1;

        $reservation->save();

        return redirect('/ammend/')->with('message', 'The reservation has been succesfully cancelled.');
    }

    public function checkDatesAvailability(Request $request){
        $checkIn = $request->checkIn;
        $checkOut = $request->checkOut; 
        $tempRooms = Room::with('roomtype')->whereNotIn('id', function($query) use($checkIn, $checkOut){
            $query->select('room_id')
                ->from(with(new Reservation)->getTable())
                ->where([['start_date', '<',$checkOut], ['end_date', '>', $checkIn]])
                ->whereOr([['start_date', '<', $checkIn],['end_date', '>', $checkOut]]);
        })->get();

        foreach($tempRooms as $room){
            if($room->id == $request->id){
                return "true";
            }
        }

        return "Sorry the room is not available at this date!";
    }

    public function getNewReservations(){
        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);

        $trans = Reservation::whereBetween('created_at', [$start , $end])->count();

        return $trans;
    }

    public function getTotalCharges(){
        $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
        $end = (new Carbon('now'))->hour(23)->minute(59)->second(59);
        $trans = Charge::whereBetween('created_at', [$start , $end])->sum('amount');

        return(number_format($trans/100));
    }

    public function recentReservations(){
        $reservations = Reservation::orderBy('created_at', 'DESC')->get()->values()->slice(0,15);

        return view('helpers.reservations-table-helper')->with('reservations', $reservations);
    }

    public function todayArrivals(){
        $reservations = Reservation::whereDate('start_date', Carbon::now()->toDateString())->get();

        return ($reservations);
    }

    public function todayArrivalsTable(){
        return view('helpers.recent-reservations-helper')->with('reservations', $this->todayArrivals());
    }

    public function todayDepartures(){
        $reservations = Reservation::whereDate('end_date', Carbon::now()->toDateString())->get();

        return ($reservations);
    }

    public function todayDeparturesTable(){
        return view('helpers.recent-reservations-helper')->with('reservations', $this->todayDepartures());
    }

    public function getReservationsOnMonth(Request $request){
        $reservations = Reservation::whereMonth('created_at', $request->month)->whereYear('created_at', $request->year)->orderBy('created_at','DESC')->get();

        return view('helpers.reservation-reports-helper')->with('reservations', $reservations);
    }

    public function getReservationsOnYear(Request $request){
        $reservations = Reservation::whereYear('created_at', $request->year)->orderBy('created_at','DESC')->get();

        return view('helpers.reservation-reports-helper')->with('reservations', $reservations);
    }

    public function getReservationsOnDate(Request $request){
        $start = Carbon::parse($request->date)->hour(0)->minute(0)->second(0);
        $end = Carbon::parse($request->date)->hour(23)->minute(59)->second(59);

        $reservations = Reservation::whereBetween('created_at', [$start , $end])->orderBy('created_at','DESC')->get();

        return view('helpers.reservation-reports-helper')->with('reservations', $reservations);
    }

    public function getCancelledReservations(){
        $reservations = Reservation::where('status', 1)->orderBy('updated_at', 'DESC')->get();

        return view('helpers.reservation-reports-helper')->with('reservations', $reservations);
    }

    public function getAllReservations(){
        $reservations = Reservation::orderBy('created_at', 'DESC')->get();
        return view('helpers.reservation-reports-helper')->with('reservations', $reservations);

    }

    public function getRoomReservation(Request $request){
        $reservations = Reservation::where('room_id' , $request->room_id)->get();
        return view('helpers.reservation-reports-helper')->with('reservations', $reservations);
    }

    public function getRoomReservationStatistics(Request $request){
        $newres = collect();
        $rooms = Room::all();

        foreach ($rooms as $key => $room) {
            $newres = $newres->push(['room_name' => $room->room_name, 'count' => count($room->reservation), 'room_type' =>$room->roomtype->roomtype_name]);
        }



        return view('helpers.room-report-helper')->with('res', $newres);
    }

    public function getAvailableRoomsToday(){
        $rooms = count(Room::all());
        $reservations = count(Reservation::where('start_date', Carbon::now()->toDateString())->get());

        return ($rooms-$reservations);
    }
}

