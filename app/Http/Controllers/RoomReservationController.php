<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;

use App\Http\Requests\GetRoomsRequest;

use App\Http\Requests\PaymentRequest;

use App\Room;

use App\Reservation;

use App\Roomtype;

use App\Ammenity;

use App\Charge as ResCharge;
 
use Carbon\Carbon;

use Stripe\Stripe;

use Stripe\Charge;

use App\Mail\ReservationConfirmation;

class RoomReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function getAvailableRooms(GetRoomsRequest $request){
        $res = new Reservation;
        $ammenities = Ammenity::all();

        $roomTypeArray = array();

        $checkIn = $request->checkIn;
        $checkOut = $request->checkOut;
        $capacity = $request->adults;

        if($request->children!=""&&$request->children>=2){
            $capacity+=round($request->children/2, 0, PHP_ROUND_HALF_DOWN);
        }

        $tempRooms = Room::with('roomtype')->whereNotIn('id', function($query) use($checkIn, $checkOut, $capacity){
            $query->select('room_id')
                ->from(with(new Reservation)->getTable())
                ->where([['start_date', '<',$checkOut], ['end_date', '>', $checkIn]])
                ->whereOr([['start_date', '<', $checkIn],['end_date', '>', $checkOut]]);
        })->get();


        $rooms = $tempRooms->groupBy('roomtype_id')->values()->all();
        $uniqueRoomTypes = $tempRooms->unique('roomtype_id')->values()->all();

        foreach($uniqueRoomTypes as $index=>$roomtype){
            $roomTypeArray['roomtypes'][$index] = $uniqueRoomTypes[$index]->roomtype;
        }

        foreach($rooms as $index=>$roomtype){
            $roomTypeArray['availableRooms'][$index] = $rooms[$index]->count();
        }

        foreach($rooms as $index=>$roomtype){
            $roomTypeArray['minPrice'][$index] = $rooms[$index]->min('price');
        }

        $date = Carbon::parse($checkIn);
        $cutoff_date = $date->subDays(7);
        $stayLength = Carbon::parse($checkOut)->diffInDays(Carbon::parse($checkIn));

        $res->start_date = $checkIn;
        $res->end_date = $checkOut;
        $res->guests = (int) $capacity;
        $res->stay_length = $stayLength;
        $res->adults = (int)$request->adults;
        $res->cutoff_date = $cutoff_date;
        if($request->children!=""){
            $res->children = (int) $request->children;
        }

        session(['step' => 2, 'reservationDetails' => $res, 'roomTypeArray' => $roomTypeArray, 'rooms' => $rooms, 'ammenities' => $ammenities]);

        return redirect()->action('ReservationController@getReservationStep', ['step' => 2]);
    }

    public function book(Request $request){
        session(['step' => 3]);

        $room = Room::with('roomtype')->find($request->room_id);
        session('reservationDetails')->room_id = $room->id;

        $stayLength = session('reservationDetails')->stay_length;

        $season = session('reservationDetails')->getSeason();

        session('reservationDetails')->setAmount($room);

        foreach(session('ammenities') as $ammenity){
            if($request->has($ammenity->ammenity_code)){
                session('reservationDetails')->res_ammenities[$ammenity->id] = $ammenity;
                session('reservationDetails')->res_ammenities[$ammenity->id]['qty'] = $request->get($ammenity->ammenity_code);
                session('reservationDetails')->amount += $ammenity->price * $request->get($ammenity->ammenity_code);
            }
        }


        $price = (session('reservationDetails')->getAmount());

        return redirect()->action('ReservationController@getReservationStep', ['step' => session('step')]);
    }

    public function confirmReservation(PaymentRequest $request){
        $reservation = session('reservationDetails');
        if(session('reservation_status')==1){
            return redirect()->action('ReservationController@getReservationStep', ['step' => 1]);
        }

        Stripe::setApiKey("sk_test_Jsg0MVddWCbfSf8r6MZfa7Tk");

        $token = $request->stripeToken;

        try {
          $charge = Charge::create(array(
            "amount" => $reservation->getCharge()*100, // Amount in cents
            "currency" => "php",
            "source" => $token,
            "description" => "Reservation Charge"
            ));
        }catch(\Stripe\Error\Card $e) {
            return($e);
        }

        $res_charge = ResCharge::create([
            'charge_code' => $charge->id,
            'amount' => $charge->amount,
        ]);

        $reservation->charge_id = $res_charge->id;
        $reservation->save();

        $reservation->confirmation_code = str_random(8).$reservation->id.str_random('2');
        $reservation->save();

        if($reservation->res_ammenities){
            foreach($reservation->res_ammenities as $ammenity){
                $reservation->ammenities()->attach($ammenity, ['qty' => $ammenity->qty]);
            }
        }

        Mail::to(
            ['to' => [
                'address' => $reservation->user->userable->email,
                'email' => $reservation->user->userable->email,
                'name' => $reservation->firstname.' '.$reservation->lastname
            ]]
            )->send(new ReservationConfirmation($reservation));

        session(['step' => 1, 'reservationDetails' => $reservation, 'reservation_status' => 1]);

        return redirect()->action('ReservationController@getReservationStep', ['step' => 5]);
    }

}
