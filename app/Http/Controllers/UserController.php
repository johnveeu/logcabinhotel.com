<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Carbon\Carbon;
class UserController extends Controller
{
	public function getNewUsers(){
		$start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
		$end = (new Carbon('now'))->hour(23)->minute(59)->second(59);

		$trans = User::whereBetween('created_at', [$start , $end])->count();

		return $trans;
	}

	public function indexOrdered($items){
		$users = User::orderBy('created_at', 'DESC')->get()->values()->slice(0, 10);

		return view('helpers.user-table-helper')->with('users', $users);
	}

	public function getAllUsers(){
		$users = User::all();

		return view('helpers.user-reports-helper')->with('users', $users)->with('type', 1);
	}

	public function getUsersCreatedAt(Request $request)
	{
		// $start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
		$start = Carbon::parse($request->date)->hour(0)->minute(0)->second(0);
		$end = Carbon::parse($request->date)->hour(23)->minute(59)->second(59);

		$users = User::whereBetween('created_at', [$start , $end])->get();

		return view('helpers.user-reports-helper')->with('users', $users)->with('type', 1);
	}
}
