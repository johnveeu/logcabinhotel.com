<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\CustomerRequest;

use App\Http\Requests\CustomerNewRequest;

use App\Http\Requests\CustomerUpdateRequest;

use App\Customer;
use App\User;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.users.customer-create');

    }

    public function storeNew(CustomerNewRequest $request){
        $customer = new Customer;

        $customer->email = $request->email;
        $customer->password = bcrypt('password');

        $customer->save();

        $user = new User;
        $user->firstname = $request->firstname;
        $user->middlename = '';
        $user->lastname = $request->lastname;
        $user->userable_id = $customer->id;
        $user->userable_type = 'App\Customer';

        $user->save();

        return redirect(url('admin/dashboard/users'))->with('success', 'Customer succesfully created');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $customer = Customer::where(['email' => $request->email])->first();

        if($customer){
            $user = User::where(['userable_id' => $customer->id, 'userable_type' => 'App\Customer'])->first();

            $user->firstname = $request->firstname;
            $user->middlename = $request->middlename;
            $user->lastname = $request->lastname;

            $user->save();
        }else{
            $customer = new Customer;

            $customer->email = $request->email;
            $customer->password = bcrypt('password');

            $customer->save();

            $user = new User;
            $user->firstname = $request->firstname;
            $user->middlename = $request->middlename;
            $user->lastname = $request->lastname;
            $user->userable_id = $customer->id;
            $user->userable_type = 'App\Customer';

            $user->save();
        }

        session('reservationDetails')->firstname = $user->firstname;
        session('reservationDetails')->middlename = $user->middlename;
        session('reservationDetails')->lastname = $user->lastname;
        session('reservationDetails')->user_id = $user->id;
        session(['step' => 4]);

        return redirect()->action('ReservationController@getReservationStep', ['step' => 4]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);

        return view('dashboard.users.customer-update')->with('customer', $customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerUpdateRequest $request, $id)
    {
        //
        $customer = Customer::find($id);

        $customer->email = $request->email;

        $user = User::find($customer->id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;

        $customer->save();
        $user->save();

        return redirect(url('admin/dashboard/users'))->with('success', 'Customer succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);

        $customer->user->delete();
        $customer->delete();

        return redirect(url('admin/dashboard/users'))->with('success', 'Customer succesfully deleted');

    }

    public function findUser(Request $request){
        $customer = Customer::with('user')->where('email', 'like', $request->email)->first();

        return($customer);
    }
}
