<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;

use App\User;

use App\Http\Requests\EmployeeNewRequest;

use App\Http\Requests\EmployeeUpdateRequest;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.users.employee-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeNewRequest $request)
    {
        $employee = new Employee;

        $employee->username = $request->username;
        $employee->password = bcrypt($request->password);

        $employee->save();

        $user = new User;
        $user->firstname = $request->firstname;
        $user->middlename = '';
        $user->lastname = $request->lastname;
        $user->userable_id = $employee->id;
        $user->userable_type = 'App\Employee';

        $user->save();

        return redirect(url('admin/dashboard/users'))->with('success', 'Employee succesfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        return view('dashboard.users.employee-update')->with('employee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request, $id)
    {
        //
        $employee = Employee::find($id);

        $employee->username = $request->username;

        $user = User::find($employee->id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;

        $employee->save();
        $user->save();

        return redirect(url('admin/dashboard/users'))->with('success', 'Employee succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);

        $employee->user->delete();
        $employee->delete();

        return redirect(url('admin/dashboard/users'))->with('success', 'Employee succesfully deleted');
    }
}
