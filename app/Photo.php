<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public $timestamps = true;

    protected $fillable = ['path', 'room_id'];
    public function room(){
    	return $this->belongsTo('App\Room', 'room_id');
    }
}














