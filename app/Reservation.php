<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class Reservation extends Model
{
    use SoftDeletes;

	protected $fillable = ['start_date', 'end_date', 'room_id', 'user_id', 'amount', 'cutoff_date', 'stay_length', 'charge_id', 'firstname',' middlename', 'lastname', 'confirmation_code', 'status', 'adults', 'children'];

    protected $dates = ['start_date', 'end_date', 'cutoff_date', 'deleted_at'];

    public $capacity, $guests, $res_ammenities;

    private $season = 3;

    public function room(){
    	return $this->belongsTo('App\Room', 'room_id');
    }

    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function charge(){
    	return $this->belongsTo('App\Charge', 'charge_id');
    }

    public function ammenities(){
        return $this->belongsToMany('App\Ammenity', 'ammenities_reservations', 'reservation_id', 'ammenity_id')->withPivot('qty');
    }

    public function getSeason(){
        $date = Carbon::parse($this->start_date);
        if((($date->month==1&&$date->day<=1)||($date->month==12&&$date->day>=16))||
            (($date->month==2))){
            $this->season = 1;
        }elseif((($date->month==6&&$date->day>=16)||($date->month==10&&$date->day<14))||(($date->month>6&&$date->month<10))){
            $this->season = 2;
        }
        return $this->season;
    }

    public function getSeasonFromDate($date){
        $date = Carbon::parse($date);
        if((($date->month==1&&$date->day<=1)||($date->month==12&&$date->day>=16))||
            (($date->month==2))){
            $this->season = 1;
        }elseif((($date->month==6&&$date->day>=16)||($date->month==10&&$date->day<14))||(($date->month>6&&$date->month<10))){
            $this->season = 2;
        }
        return $this->season;
    }


    public function setAmount($room){
        $season = $this->season;

        if($season==1)
        {
            $this->amount = $this->stay_length * $room->peakseason_rate;
        }elseif($season==2)
        {
            $this->amount = $this->stay_length * $room->offseason_rate;
        }else
        {
            $this->amount = $this->stay_length * $room->midseason_rate;
        }
    }

    public function getAdults(){
        return $this->adults;
    }


    public function getAmount(){
        return $this->amount;
    }

    public function formatDate($date){
        $fDate = Carbon::parse($date);

        return $fDate->format('l\\, F j \\, Y'); 
    }

    public function formatNumber($number){
        return number_format($number);
    }

    public function formatStartDate(){
        return Carbon::parse($this->start_date)->format('l\\, F j \\, Y');
    }

    public function formatEndDate(){
        return Carbon::parse($this->end_date)->format('l\\, F j \\, Y');
    }

    public function formatStartDate2(){
        return Carbon::parse($this->start_date)->format('F j \\, Y');
    }

    public function formatEndDate2(){
        return Carbon::parse($this->end_date)->format('F j \\, Y');
    }

    public function formatPrice(){
        return number_format($this->amount);
    }

    public function getCharge(){
        return($this->amount*0.50);
    }

    public function getName(){
        return($this->firstname.' '.$this->lastname);
    }

    public function getGuests(){
        return($this->adults + $this->children);
    }

    public function getAmmenity($ammenity_id){
        return $this->ammenities()->where('ammenity_id', $ammenity_id);
    }

}
