<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //
    public function roomtype(){
    	return $this->belongsTo('App\Roomtype');
    }

    public function reservation(){
    	return $this->hasMany('App\Reservation', 'room_id');
    }

    public function photos(){
    	return $this->hasMany('App\Photo');
    }

    public function getDisplayRate($season){
        if($season == 1){
            return $this->peakseason_rate;
        }elseif($season == 2){
            return $this->offseason_rate;
        }
        
        return $this->midseason_rate;
    }
}


