<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //

    public function userable()
    {
        return $this->morphTo();
    }

    public function reservation(){
    	return $this->hasMany('App\Reservation');
    }

    public function formatCreateDate(){
        $fDate = Carbon::parse($this->created_at);

        return $fDate->toDayDateTimeString();
    }


}
