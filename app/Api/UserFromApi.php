<?php 
	namespace App\Api;

	use App\User;
	use GuzzleHttp\Client;	
	use GuzzleHttp\Exception\ClientException;


	class UserFromApi{
		protected $user;

		protected $client;

		protected $attributes = [];

		public function __construct(User $user, Client $client){
			$this->user = $user;
			$this->client = $client;
		}

		public function queryApiUserBy($field){
			$attributes = $this->getApiUser();

			foreach ($attributes as $attrubute) {
				if($attribute==$field){
					$this->attribute = $attributes;

					break;
				}
			}

			return $this->attributes ?: null;

		}

		protected function getApiUser(){
			try{
				$reponse = $this->getAccessTokenFromApi();
				$user = $this->client->request('GET', 'http://logcabinhotel.com/api/user',[
							'headers' => [
								'Content-Type' => 'application/json',
								'Authorization' => 'Bearer '.$response['access_token']
							]

						]);
			}catch(ClientException $e){
				return $e->getResponse();
			}

			return json_decode($user->getBody(), true);
		}

		protected function getAccessTokenFromApi(){
			$token = $this->client->post('http://logcabinhotel.com/oauth/token', [
						'form_params' => [
							'client_id'		=> config('services.passport.client_id'),
							'client_secret' => config('services.passport.client_secret'),
							'grant_type' 	=> config('services.passport.grant_type'),
							'username'  	=> session('username'),
							'password' 		=> session('password'),
							'scope' 		=> '*'
						]
					]);
			return json_decode($token->getBody(), true);
		}

		protected function createNewUser($attributes){
			if(!is_null($attributes)) return new User($attributes);
		}



	}


 ?>