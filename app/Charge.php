<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    //
    protected $fillable = ['charge_code', 'amount'];

    public function reservation(){
    	return $this->hasOne('App\Reservation');
    }
}
