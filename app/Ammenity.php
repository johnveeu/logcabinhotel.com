<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ammenity extends Model
{
    //
    public function reservations(){
    	return $this->belongsToMany('App\Reservation')->withPivot('qty');
    }

    public function getTotal(){
		$this->price*$this->qty;    	
    }
}
