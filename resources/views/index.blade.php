@extends('layouts.master')

@section('title', 'Log Cabin Hotel')

@section('stylesheets')
	<link rel="stylesheet" type="text/css" href="/css/main.css">
@endsection

@section('contents')
		<div class="carousel slide w-100 h-100" id="splash-slider" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#splash-slider" data-slide-to="0" class="active"></li>
			    <li data-target="#splash-slider" data-slide-to="1"></li>
			    <li data-target="#splash-slider" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner" role="listbox">
			    <div class="carousel-item active item1  ">
			      <div class="carousel-caption d-block mx-auto align-middle">
			    	<h2><span class="spec">Welcome to the SAFARI LODGE</span></h2>
			    	<p>Come and stay with us! Experience the beauty and relax. Feel the difference.</p>
			      </div>
			    </div>
			    <div class=" carousel-item item2">
			      <div class="carousel-caption d-md-block mx-auto align-middle">
			        <h2><span class="spec">Perfect place for your Baguio vacation</span></h2>
			    	<p>With great location, service, and stay. Come and feel comfortable.</p>
			      </div>
			    </div>
			    <div class=" carousel-item item3">
			      <div class="carousel-caption d-md-block mx-auto align-middle">
			        <h2><span class="spec">Feel like your home</span></h2>
			    	<p>Bring your family and discover the goodness and ambiance of log in hotel. We put a smile back on your face.</p>
			    	<button></button>
			      </div>
			    </div>
	  		</div>
		</div>
	
	<div class="booking-cta">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h2 class="bn-title">
						Search Rooms <span class="cta-title-p"> For rates & availability</span>
					</h2>
				</div>
				<div class="col-md-9">
					<div class="book-now">
						<form class="bn-form" method="POST" action='{{url("/get_available_rooms")}}'>
						{{csrf_field()}}


							
							<div class="row">
								<div class="col-md-3 col-xs-6">
									@if ($errors->has('checkIn'))
		                                <span class="help-block">
		                                    <strong>{{ $errors->first('checkIn') }}</strong>
		                                </span>
		                            @endif
									<div class="input-group{{ $errors->has('checkIn') ? ' has-error' : '' }}">
										<span class="input-group-addon">
								 			<i class="fa fa-calendar"></i>
								 		</span>
								 		<input class="form-control date" placeholder="check-in" id="check-in" name="checkIn" value="{{ old('checkIn') }}">
								 	</div>
								</div>

								<div class="col-md-3 col-xs-6">
									<div class="input-group{{ $errors->has('checkOut') ? ' has-error' : '' }}">
										<span class="input-group-addon">
								 			<i class="fa fa-calendar"></i>
								 		</span>
								 		<input class="form-control checkout-date" placeholder="check-out" id="check-out" name="checkOut" value="{{ old('checkOut') }}">
								 	</div>
								</div>

								<div class="col-md-3">
									<div class="row">
										<div class="col-sm-6 w-50">
											<div class="pax-adult" name="adult" tabindex="0">
												<select class="pax-count" name="adults">
													<option value="">Adult</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
												</select>
											</div>
										</div>

										<div class="col-sm-6 w-50">
											<div class="pax-child " tabindex="0">
												<select class="pax-count" name="children">
													<option value="" >Child</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
												</select>	
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<input class="btn main-btn btn-block" type="submit" id="book-submit" value="SUBMIT">
								</div>
							</div> 
						</form>
					</div>
				</div>		
			</div>
		</div>
	</div>

	<div class="second-title">
		<h2> Our Best Rooms</h2>
		<p>These rooms are chosen by our customers</p>		
	</div>

	<div class="container">
		<div class="row ">
			<div class="col-sm-4">
				 <div class="hovereffect">
					<img src="/img/rooms/TBS/GOPR0429-min.JPG" class="room-img-hover">
					<div class="overlay">
						<h2>The Botanical Suite</h2>
		   				<p class="info" href="#">A Warm and Relaxing ambiance for your family.</p>
			        </div>
			    </div>
			</div>
			<div class="col-sm-4 ">
				 <div class="hovereffect">
					<img src="/img/rooms/TFS/GOPR0489-min.JPG" class="room-img-hover">
					<div class="overlay">
						<h2>The Fireplace Suite</h2>
		   				<p class="info" href="#">A Warm and Relaxing ambiance for your family.</p>
			        </div>
			    </div>
			</div>
			<div class="col-sm-4 ">
				 <div class="hovereffect">
					<img src="/img/rooms/35/GOPR0297-min.jpg" class="room-img-hover">
					<div class="overlay">
						<h2>Family Rooms</h2>
		   				<p class="info" href="#">A Warm and Relaxing ambiance for your family.</p>
			        </div>
			    </div>
			</div>		
		</div>	
	</div>


	<div class="index-about parallax ">
		<div class="container jumbotron-about">
			<h3 class="text-xs-center"> ABOUT </h3>
				<br>
			<div class="row">
				<div class="col-md-7 ">
					<h1 class="about-title text-xs-center">"Safari Lodge by Log Cabin Hotel"</h1>
					<br>
					<p class="index-about-p"> 
						is 250 km from Manila via seanic Kennon Road. By car, take the North Expressway at Balintawak and get off at Dau exit. Pass Tarlac, Urdaneta, and Pozorrubio. Before reaching Rosario, take the junction up to Baguio (Kennon Road)

						Note: If Kennon Road is not passable due to landslides, there are two other routes going to Baguio: Marcos Highway, accessible from Agoo, La Union or Naguilian Road, accesible from Bauang, also in La Union.
					</p>
					<p class="index-about-p">
						The Safari Lodge is located amidst pine trees facing Botanical  Garden. It has 16 rooms    to    accommodate its guest comprising of big luxurious family rooms, standard and budget  rooms. Additional beds or mattresses can be added at a minimum surcharge.                                                   

						Dining facilities at the Lodge includes a unique main restaurant decorated by trophies of African and Philippine wildlife animals hunted by the founder (Celso Tuason). Part of his  collection includes life size elephant head, rhino and buffalo,a full size leopard   and lions.All this adding to a colorful and charming ambiance.  One may also dine at the  terrace/veranda or at the patio gardens.
					</p>
					<p class="index-about-p">
						Rooms are furnished with an LED TV and come with an attached bathroom which provides shower facilities. Select rooms include a sofa. Safari Lodge offers laundry and massage services on request at a charge. Newspapers are provided for reads while a designated smoking area is available. 

						Its main restaurant serving local cuisine is decorated with trophies of African and Filipino wildlife animals, while room service is available. Guests may also enjoy acoustic music at the Sports Bar or even request bonfires for family bonding and team building.
					</p>

				</div>
				<div class="col-md-5">
					<div class="about-index">
						<img src="../img/about-img-index.png">
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="ammenities">
		<div class="second-title">
			<h2> Hotel Facilities</h2>
			<p>These best rooms chosen by our customers</p>
		</div>

		<div class="container">
			<div class="row facilities-row">
				<div class="col-sm-3">
					<div class="facilities">
						<div class="facilities-title">
							<h3><i class="fa fa-wifi"> </i> Free Wifi </h3>
							<p> Free wifi is accesible within the compound of the hotel </p>
						</div>
					</div>	
				</div>
				<div class="col-sm-3">
					<div class="facilities">
						<div class="facilities-title">
							<h3><i class="fa fa-shield"> </i> 24-Hour Security </h3>
							<p> Assures your safety espcially when you are a sleep. </p>
						</div>
					</div>	
				</div>
				<div class="col-sm-3">
					<div class="facilities">
						<div class="facilities-title">
							<h3><i class="fa fa-cutlery"> </i> Restaurant </h3>
							<p> Assures your safety espcially when you are a sleep. </p>
						</div>
					</div>	
				</div>
				<div class="col-sm-3">
					<div class="facilities">
						<div class="facilities-title">
							<h3><i class="fa fa-coffee"> </i> Café </h3>
							<p> Assures your safety espcially when you are a sleep. </p>
						</div>
					</div>	
				</div>
			</div>	
		</div>

		
	</div>	

		
		<div class="index-contact">
			<div class="container-fluid jumbotron-about">
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<h5 class="contact-title"> We are located at</h5>
							<address><i class="fa fa-map-marker"> </i><strong>
								191 Leonard Wood Rd., Baguio City, Philippines </strong> <br><br></address>
							<address>
						<div class="social-media">
							<h5 class="contact-title"> You can visit our pages on social media sites</h5>
								<a href="#" class="btn btn-social-icon"><i class="fa fa-facebook-official fa-3x"></i></a> 
								<a href="#" class="btn btn-social-icon"><i class="fa fa-instagram fa-3x"></i></a>
								<a href="#" class="btn btn-social-icon"><i class="fa fa-twitter-square fa-3x"></i></a>  
								<a href="#" class="btn btn-social-icon"><i class="fa fa-tripadvisor fa-3x"></i></a> <a href="#" class="btn btn-social-icon"><i class="fa fa-tumblr-square fa-3x"></i></a> 
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="contact-us">
							 	Mobile Numbers :<br>										
							 		<strong><i class="fa fa-mobile pl-2"> </i> +63 917 632 8658 (Globe)<br>
									<i class="fa fa-mobile pl-2"> </i> +63 925 310 5682 (Sun)<br>
									<i class="fa fa-mobile pl-2"> </i> +63 998 842 5301 (Smart)
									</strong>										
							</address>
							<address>
								Telephone Number: <br>
									<i class="fa fa-phone pl-2"> </i>  (074) 123-4567

							</address>
							<address>
								Email address:<br>
									<a href="mailto:#" class="email pa-3"><strong>logcabinhotel_fo@hotmail.com</strong></a>
							</address>
						</div>	
					</div>
					<div class="col-md-4 col-sm-6 ">
						<div class="form-group">
							<label for="Name">Name</label>
							<input type="text" class="form-control" placeholder="Name" id="Name">
						</div>
						<div class="form-group">
							<label for="Name">E-Mail Address</label>
							<input type="text" class="form-control" placeholder="Your E-mail" id="Email">
							<small class="form-text text-muted">We'll never share your email with anyone else.</small>
						</div>
						<div class="form-group">
			                <label for="msg"> Message </label>
			                <textarea name="message" rows="3" id="msg" class="form-control" placeholder="Message"></textarea>
			            </div>
			            <div class="col-md-12 text-xs-center">
			              <button type="submit" class="main-btn" id="btnContactUs">Send Message</button>    
			            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		$('.navbar .home').addClass('active');
	</script>

	<script>
	$(document).ready(function() {
		var minDate;
		$checkoutdate =  flatpickr(".checkout-date",{
	    	altInput: true,
	    	altFormat: "F j, Y",
		});
		flatpickr('.date',{
			minDate: new Date().fp_incr(1),
			altInput: true,
	    	altFormat: "F j, Y",
			onChange: function(dateStr){
				$checkoutdate.setDate(new Date(dateStr).fp_incr(1));
				$checkoutdate.set('minDate', new Date(dateStr).fp_incr(1));
			}
		});


	});
</script>
@endsection