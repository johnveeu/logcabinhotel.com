<!DOCTYPE html>
<html>
<head>
    <title>@yield('title') - Safari Lodge</title>
     <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/flatpickr.min.css">
    <link rel="stylesheet" type="text/css" href="/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/css/datatables.min.css">

    @yield('stylesheets')
</head>
<body class="dashboard-admin">
        <nav class="navbar navbar-inverse navbar-toggleable-md background-main fixed-top mb-0">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
            <a class="navbar-brand" href="/admin/home">
                Safari Lodge - Admin
            </a>
            <div class="collapse navbar-collapse float-xs-right">
                <ul class="user-menu nav justify-content-end ml-auto my-md-0">
                    <li class="dropdown float-xs-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> {{Auth::user()->user->firstname}}<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu" style="right: 0; left: auto;">
                            @if(Auth::user()->user->userable_type == "App\Admin")
                            <li>
                                <a href='{{url("users")}}' class="dropdown-item">User Management</a>
                            </li>
                            @endif
                            <li>
                                <a href='{{url("reservations")}}' class="dropdown-item">Reservations</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container wrap">
            <div class="row">
                @include('dashboard.sidebar')

                @yield('contents')
            </div>
        </div>
    <script src="/js/jquery.js"></script>
    <script src="/js/tether.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/flatpickr.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/smoothscroll.js"></script>
    <script src="/js/date.js"></script>
    <script src="/js/selectize.js"></script>
    <script src="/js/datatables.min.js"></script>
    <script src="/js/dataTables.buttons.min.js"></script>
    <script src="/js/buttons.print.min.js"></script>

    
    @yield('scripts')
</body>
</html>