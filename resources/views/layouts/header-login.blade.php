<header class="transparent-header navbar-toggleable-md navbar-inverse navbar navbar-default w-100">
  <button class="navbar-toggler navbar-toggler-right my-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon main-color"></span>
  </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class= "nav navbar-nav ml-auto">
      <li class="nav-item home">
        <a class="nav-link btn-sm" href="/"> Safari Lodge <span class="sr-only">(current) </span></a>
      </li>
      <li class="nav-item rooms">
        <a href="/rooms" class="nav-link btn-sm">Employee Log-in</a>
      </li>
      <li class="nav-item reservation dropdown">
        <a class="nav-link btn-sm dropdown-toggle" data-toggle="dropdown" href="/reseration/step/1" role="button" aria-haspopup="true" aria-expanded="false">Reservation</a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="/reservation/step/1">Reserve a room</a>
          <a class="dropdown-item" href="/ammend/">Edit a Reservation</a>
        </div>
      </li>
    </ul>
    </div>
    
 
</header>

