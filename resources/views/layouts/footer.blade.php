<footer class="footer">
  <div class="container m-1">
      <div class="row">
        <div class="col-md-6">                      
            <ul class="nav nav-inline nav-footer" >
              <li class="nav-item">
                <a class="nav-link" href="/home">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/reservation">Reservation</a>
              </li>
              @if(Auth::check())

              @else
              <li class="nav-item">
                <a class="nav-link" href="/employee/login">Employee Log-in</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/admin/login">Admin Log-in</a>
              </li>
              @endif
            </ul>
        </div>
         <div class="col-md-6 text-center">
            <p >
              <i class="fa fa-copyright"> </i>
                <script type="text/javascript">
                var d = new Date()
                document.write(d.getFullYear())
                </script>|<a href="#top" class="textcolor"></a>
                  <small>UMINGA, CALSIE, MINONG | ALL RIGHTS RESERVED</small>      
            </p> 
        </div>     
      </div>
  </div>
    
     	
</footer>