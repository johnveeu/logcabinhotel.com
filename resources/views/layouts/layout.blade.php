<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" href="css/boottrap.css">
	</head>
	<body>
		<h1 class="text-xs-center">Hello World</h1>

		<!-- jQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/js/bootstrap.js"></script>
	</body>
</html>