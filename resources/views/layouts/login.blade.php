<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')Login - Safari Lodge</title>
     <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/flatpickr.min.css">

    @yield('stylesheets')
</head>
<body>
	@include('layouts.header-login')
    @yield('contents')
    <script src="/js/jquery.js"></script>
    <script src="/js/tether.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/flatpickr.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/smoothscroll.js"></script>
    
    
    @yield('scripts')
</body>
</html>