<!DOCTYPE html>
<html>
<head >
    <title>@yield('title') - Safari Lodge</title>
     <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/flatpickr.min.css">

    @yield('stylesheets')
</head>
<body style="width: 100%;">
    <div class="container-fluid" style="background-color: #132027">
        <div class="email-header" style="padding:10px">
            <p style="text-align: center; font-weight: bold; font-size: 40px; color: white;" >SAFARI LODGE</p>
        </div>
    </div>
    <div class="message container-fluid" style="padding: 30px;">
        <div style="font-size: 24px;">
            <p><strong>Dear {{$emailDetails->firstname}} {{$emailDetails->middlename}} {{$emailDetails->lastname}}</strong></p>
            <p style="font-size: 20px;">You have successfuly moved your reservation. Thank you for your reservation at Safari Lodge.</p>
            <hr>  
            <p style="font-size: 20px;">Here are the details of your reservation: </p>
        </div>
        <div style="background-color: #132027; padding: 30px; color: white; font-size: 20px;" >
            <p style="font-size: 28px; font-weight: bold;"> {{$emailDetails->firstname}} {{$emailDetails->middlename}} {{$emailDetails->lastname}}</p>
            <hr style="background-color:#967a50">
            <p><span style="color: #967a50; font-weight: bold;">DATES: </span>{{$emailDetails->formatDate($emailDetails->start_date)}} to {{$emailDetails->formatDate($emailDetails->end_date)}} ({{$emailDetails->stay_length}} @if($emailDetails->stay_length==1) NIGHT @else NIGHTS @endif)</p>
             <p><span style="color: #967a50; font-weight: bold;">CUTOFF DATE:  </span>{{$emailDetails->formatDate($emailDetails->cutoff_date)}}</p>
            <p><span style="color: #967a50; font-weight: bold;">ROOM: </span>{{$emailDetails->room->room_name}}</p>
            <p><span style="color: #967a50; font-weight: bold;">TOTAL: </span>Php {{$emailDetails->formatNumber($emailDetails->amount)}}</p>
            <p><span style="color: #967a50; font-weight: bold;">RESERVATION CHARGE:Php </span>{{$emailDetails->formatNumber($emailDetails->charge->amount)}}</p>
            <p><span style="color: #967a50; font-weight: bold;">RESERVATION CODE:  </span>{{$emailDetails->confirmation_code}}</p>
            <hr style="background-color:#967a50">
            <p class="text-xs-center" style="font-weight: bold;">Travel safely and we look forward to accomidating you.</p>
            <hr style="background-color:#967a50">
        </div>
        <div class="email-footer" style="padding: 30px;">
            <p>
                <strong>Cancellation: </strong>Cancellation is free if you cancel your reservation before 7 days before your check-in time. Failure to cancel your reservation within the given period be charged 100% of the reservation charge. If you wish to cancel the reservation please go to https://logcabinhotel.com/reservation/cancel and follow the necessary steps.
            </p>
            <p>
                <strong>Modification: </strong>You are allowed to edit a reservation only before 7 days before your check-in time. 
            </p>
        </div>
        
    </div>

  </div>
    
</body>
</html>