@extends('layouts.master')

@section('title', 'Reservation')

@section('contents')
	<section class="reservation">
		<div class="page-title fixed-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>RESERVATION</h2>
						<p>Planning for a vacation? Log Cabin is the best place to stay.<br><strong> Book now!</strong></p>
					</div>
				</div>
			</div>
		</div>

		<div class="page">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a href="#" id="reservation-date-tab" class="nav-link active">
									<span class="tab-num">1</span>
									<span class="bar"></span>
									Reservation Date
								</a>
							</li>

							<li class="nav-item">
								<a href="#avail-rooms" id="room-tab" class="nav-link">
									<span class="tab-num">2</span>
									<span class="bar"></span>
									Search & Select Room
								</a>
							</li>

							<li class="nav-item">
								<a href="#reservation-info"  id="reservation-info-tab" class="nav-link">
									<span class="tab-num">3</span>
									<span class="bar"></span>
									Reservation Info
								</a>
							</li>

							<li class="nav-item">
								<a href="#payment-info" id="payment-info-tab" class="nav-link">
									<span class="tab-num">4</span>
									<span class="bar"></span>
									Payment Details
								</a>
							</li>

							<li class="nav-item">
								<a href="#thank-you" id="thank-you-tab" class="nav-link">
									<span class="tab-num">5</span>
									Thank you!
								</a>
							</li>
						</ul>

						<div class="search-room">
							<div class="check-availability">
								<div class="ca-title-container">
									<h2 class="ca-title title">
										Search for Rooms
									</h2>
								</div>

								<form class="ca-form">
									<div class="row">
										<div class="col-md-5 offset-md-1">
											<div class="input-group">
												<span class="input-group-addon">
										 			<i class="fa fa-calendar"></i>
										 		</span>
										 		<input class="form-control date" placeholder="check-in" id="check-in" name="checkIn">
										 	</div>
										</div>

										<div class="col-md-5">
											<div class="input-group">
												<span class="input-group-addon">
										 			<i class="fa fa-calendar"></i>
										 		</span>
										 		<input class="form-control date" placeholder="check-out" id="check-out" name="checkOut">
										 	</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-5 offset-md-1">
											<div class="input-group">
												<span class="input-group-addon">
										 			<i class="fa fa-user"></i>
										 		</span>
										 		<input class="form-control" placeholder="number of adults" id="adults" name="adults">
										 	</div>
										</div>

										<div class="col-md-5">
											<div class="input-group">
												<span class="input-group-addon">
										 			<i class="fa fa-child"></i>
										 		</span>
										 		<input class="form-control" placeholder="number of children" id="children" name="children">
										 	</div>
										</div>

									</div>

									<div class="row">
										<div class="col-md-3 btn-submit-container">
											<input type="button" class="main-btn btn-block" value="SUBMIT" role="button" id="ca-submit"></input>
										</div>
									</div>	
									</form>
									</div>
								
									<div class="available-rooms" id="available-rooms">
										
									</div>
															
								</div>
							</div>

						{{-- <div class="tab-content">
							<div class="tab-pane fade in active" id="avail-rooms" role="tabpanel">
								<div class="search-room">
									<div class="check-availability">
										<div class="ca-title-container">
											<h2 class="ca-title title">
												Search for Rooms
											</h2>
											<p>Check for available rooms.</p>
										</div>

										<form class="ca-form">
											<div class="row">
												<div class="col-md-5 offset-md-1">
													<div class="input-group">
														<span class="input-group-addon">
												 			<i class="fa fa-calendar"></i>
												 		</span>
												 		<input class="form-control date" placeholder="check-in" id="check-in" name="checkIn">
												 	</div>
												</div>

												<div class="col-md-5">
													<div class="input-group">
														<span class="input-group-addon">
												 			<i class="fa fa-calendar"></i>
												 		</span>
												 		<input class="form-control date" placeholder="check-out" id="check-out" name="checkOut">
												 	</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-5 offset-md-1">
													<div class="input-group">
														<span class="input-group-addon">
												 			<i class="fa fa-user"></i>
												 		</span>
												 		<input class="form-control" placeholder="number of adults" id="adults" name="adults">
												 	</div>
												</div>

												<div class="col-md-5">
													<div class="input-group">
														<span class="input-group-addon">
												 			<i class="fa fa-child"></i>
												 		</span>
												 		<input class="form-control" placeholder="number of children" id="children" name="children">
												 	</div>
												</div>

											</div>

											<div class="row">
												<div class="col-md-3 btn-submit-container">
													<input type="button" class="main-btn btn-block" data-toggle="button" aria-pressed="false" autocomplete="off" value="SUBMIT" role="button" id="ca-submit"></input>
												</div>
											</div>	
											</form>
											</div>
										
											<div class="available-rooms" id="available-rooms">
												
											</div>
																	
										</div>
									</div>
							<div class="tab-pane" id="reservation-info" role="tabpanel">
								<div class="reservation-information">
									<h2 class="page-title-center">Personal Information</h2>
									<div class="row">
										<form id="personal-information-form" method="POST">
											{{csrf_field()}}	
											<div class="col-md-8">
												<div class="row">
													<div class="col-md-4">
														<div class="res-info-input">
															<input type="text" name="ri-first-name" class="form-control" placeholder="first name">
														</div>
													</div>
													<div class="col-md-4">
														<div class="res-info-input">
															<input type="text" name="ri-middle-name" class="form-control" placeholder ="middle name">
														</div>
													</div>
													<div class="col-md-4">
														<div class="res-info-input">
															<input type="text" name="ri-last-name" class="form-control" placeholder="last name">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="res-info-input">
															<input type="text" name="ri-email" class="form-control" placeholder="email address" >
														</div>
													</div>
													<div class="col-md-6">
														<div class="res-info-input">
															<input type="text" name="ri-contact-number" class="form-control" placeholder="contact number">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="res-info-input">
															<textarea name="" name="ri-requests" cols=30 rows=10 class="form-control" form ="personal-infomation-form" ></textarea>
														</div>
													</div> 
												</div>
												<div class="row">
													<div class="col-md-3">
														<button class="btn main-btn btn-block" id="ri-proceed">Proceed</button>
													</div>
												</div>	
											</div>
										</form>
										
										<div class="col-md-4">
											<div class="selected-room-container" id="selected-room-container">
											</div>

										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="payment-info" role="tabpanel">
								<h2 class="page-title-center">Payment Information</h2>
								<div class="row">
									<div class="col-md-8">
										<h3 class="page-title-left">Card Details</h3>
										<form method="POST" id="payment-form">
											{{csrf_field()}}
											<span class="payment-errors"></span>
											<div class="row">
												<div class="col-md-6">
													<div class="res-info-input">
														<input type="text" name="" class="form-control" placeholder="card number" data-stripe="number" size="20">
													</div>
												</div>
												<div class="col-md-6">
													<div class="res-info-input">
														<input type="text" name="" class="form-control" placeholder="cvc" data-stripe="cvc" size="4">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="res-info-input">
														<input type="text" name="" class="form-control" placeholder="month of expiration" size="2" data-stripe="exp_month">
													</div>
												</div>
												<div class="col-md-6">
													<div class="res-info-input">
														<input type="text" name="" class="form-control" placeholder="year of expiration" size="2" data-stripe="exp_year">
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-3 btn-submit-container">
													<button type="button" class="btn main-btn btn-block submit" id="submit-payment">Pay!</button>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="selected-room-container" id="selected-room-container">
											</div>
										</div>
										</form>
								</div>
							</div>
							<div class="tab-pane fade" id="thank-you" role="tabpanel">
							</div>
						</div> --}}
					</div>
				</div>
			</div>
		</div>
		<div class="results">	</div>
	</section>

@endsection

@section('scripts')
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript">
	  Stripe.setPublishableKey('pk_test_Z5f7qKe3xfrQbKHSurxlxps5');
	</script>
	<script>
		$(function() {
		  var $form = $('#payment-form');
		  $('#submit-payment').click(function(event) {
		  	event.preventDefault();
		    // Disable the submit button to prevent repeated clicks:
		    $form.find('.submit').attr('disabled', true);

		    // Request a token from Stripe:
		    Stripe.card.createToken($form, stripeResponseHandler);

		    // Prevent the form from being submittedkey: "value", 
		  });

		  function stripeResponseHandler(status, response) {
			  var $form = $('#payment-form');

			  if (response.error) { // Problem!

			    // Show the errors on the form:
			    $form.find('.payment-errors').text(response.error.message);
			    $form.find('.submit').attr('disabled', false); // Re-enable submission

			  } else { // Token was created!

			    // Get the token ID:
			    var token = response.id;
			    console.log(token);

			    // Insert the token ID into the form so it gets submitted to the server:
			    $form.append($('<input type="hidden" name="stripeToken">').val(token));

			    // Submit the form:

		    	$.ajaxSetup({
					headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
			        type: 'POST',
			        url: '/payreservation', 
			        data: {token: token},
			      	success: function(response){
			      		console.log(response);
					  }
				});
			}
		}
	});

	</script>

@endsection
