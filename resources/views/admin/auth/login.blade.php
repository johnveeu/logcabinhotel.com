@extends('layouts.login')

@section('contents')
<section class="login-admin mt-5">
      <div class="container pt-5">
        <form class="form-login-admin" role="form" method="POST" action="{{ url('/admin/login') }}">
            {{ csrf_field() }}
            <h2 class="text-center label">SAFARI LODGE <span>Admin</span></h2>
            <hr>
            <div class="{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="" >Username</label>
                
               <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" autofocus placeholder="USERNAME">
               @if ($errors->has('username'))
                    <div class="alert alert-danger mt-2" role="alert">
                        <strong>Oops!</strong> {{$errors->first('username')}}
                    </div>
                @endif
            </div>
            

            <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                 <label for="password" class="{{ $errors->has('password') ? ' has-error' : '' }}" ">Password</label>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <input type="password" id="password" class="form-control" placeholder="PASSWORD" name="password" required autofocus="">
            </div>
            <div class="btn-admin pt-1 mt-3">
                <button class="btn main-btn btn-block" type="submit">SIGN IN</button> 
                <label>
                    <input type="checkbox" value="remember"> Remember me
                </label>
            </div>
                
          </form>
      </div>

</section>

@endsection
