@extends('layouts.dashboard')

@section('title', 'Users - Dashboard')

@section('contents')
	<div class="col-md-10 offset-md-2">
		<div class="row">
			<div class="col-md-12">
				<h2 class="page-header">USERS</h2>
			</div>
		</div>
			<div class="row">
				<div class="col-md-12">
					@if(session()->has('success'))
						<div class="alert alert-dismissible alert-success" role="alert" style="">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
						 	 <strong>Well done!</strong> {{session('success')}}
						</div>
					@endif
				</div>
			</div>
			<div class="row mt-3">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<strong>CUSTOMERS</strong>
							<a href='{{url("admin/dashboard/customers/create")}}' class=""><strong><i class="fa fa-plus float-sm-right my-auto"></i></strong></a>
						</div>
						<div class="card-block">
							<table class="table-striped table user-table" id="customer-table">
								<thead>
									<tr>
										<th>Email</th>
										<th>Firstname</th>
										<th>Lastname</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									@foreach($customers as $customer)
										<tr>
											<th>{{$customer->userable->email}}</th>
											<th>{{$customer->firstname}}</th>
											<th>{{$customer->lastname}}</th>	
											<th><a class="btn main-btn" href='{{url("admin/dashboard/customers/$customer->userable_id/edit")}}' role="button"><i class="fa fa-pencil"></i></a>
											<a class="btn main-btn" data-form-url = '{{url("admin/dashboard/customers/$customer->userable_id")}}' data-toggle="modal" href="#modal-delete"><i class="fa-times fa"></i></a></th>
										</tr>
									@endforeach
								</tbody>
					
							</table>
						</div>
					</div>
				</div>
			</div>									

			<div class="row mt-3">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<strong>EMPLOYEES</strong>
							<a href='{{url("admin/dashboard/employees/create")}}'><strong><i class="fa fa-plus float-sm-right"></i></strong></a>
						</div>
						<div class="card-block">
							
							<table class="table-striped table user-table" id="employee-table">
								<thead>
									<tr>
										<th>Username</th>
										<th>Firstname</th>
										<th>Lastname</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									@foreach($employees as $employee)
										<tr>
											<th>{{$employee->userable->username}}</th>
											<th>{{$employee->firstname}}</th>
											<th>{{$employee->lastname}}</th>	
											<th><a class="btn main-btn" href='{{url("admin/dashboard/employees/$employee->userable_id/edit")}}' role="button"><i class="fa fa-pencil"></i></a>
											<a class="btn main-btn" data-form-url = '{{url("admin/dashboard/employees/$employee->userable_id")}}' data-toggle="modal" href="#modal-delete"><i class="fa-times fa"></i></a></th>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="row mt-3">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<strong>ADMINS</strong>
							<a href='{{url("admin/dashboard/admins/create")}}'><strong><i class="fa fa-plus float-sm-right"></i></strong></a>
						</div>
						<div class="card-block">
							<table class="table-striped table user-table" id="admin-table">
								<thead>
									<tr>
										<th>Username</th>
										<th>Firstname</th>
										<th>Lastname</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									@foreach($admins as $admin)
										<tr>
											<th>{{$admin->userable->username}}</th>
											<th>{{$admin->firstname}}</th>
											<th>{{$admin->lastname}}</th>	
											<th><a class="btn main-btn" href='{{url("admin/dashboard/admins/$admin->userable_id/edit")}}' role="button"><i class="fa fa-pencil"></i></a>
											<a class="btn main-btn" data-form-url = '{{url("admin/dashboard/admins/$admin->userable_id")}}' data-toggle="modal" href="#modal-delete"><i class="fa-times fa"></i></a></th>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
	</div>

	<div class="modal modal-cancel fade" id="modal-delete">
			<div class="modal-dialog modal-lg" role="document" style="max-width: 600px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" style="color: #fff">&times;</span>
							<span class="sr-only">BACK</span>
						</button>
						<h4 class="modal-title" style="color: #967a50">DELETE USER</h4>
					</div>
					<div class="modal-body">
						<p>&nbsp; &nbsp; &nbsp;Are you sure you want to delete this record? This process refunds the customer's reservation charge and deletes the record from the database. This process is irreversible.</p>
						<form action='' method="POST" id="form-cancel">
							{{csrf_field()}}
							{{method_field('DELETE')}}
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn main-btn" data-dismiss="modal">BACK</button>
						<input class="btn main-btn" type="SUBMIT" value="PROCEEED" form="form-cancel">
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	
@endsection

@section('scripts')
	<script>
		$('#modal-delete').on('show.bs.modal', function (event){
            var trigger = $(event.relatedTarget);
            var formUrl = trigger.data('form-url');

            var modal = $(this);

            modal.find('#form-cancel').attr('action', formUrl);

        });
		$('.sidebar .users').addClass('active');
		$(document).ready(function(){
			$('#customer-table').DataTable();
			$('#admin-table').DataTable();
			$('#employee-table').DataTable();
			$('#customer-table_length select').addClass('custom-select');
			$('#admin-table_length select').addClass('custom-select');
			$('#employee-table_length select').addClass('custom-select');

		});
	</script>
@endsection