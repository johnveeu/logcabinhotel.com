@extends('layouts.dashboard')

@section('title', 'Employee - Update')

@section('contents')
	<div class="col-md-10 offset-md-2">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong>Edit Employee</strong>
					</div>
					<div class="card-block">
						<div class="card-title text-center"><h3>ENTER EMPLOYEE DETAILS</h3></div>
						<form action='{{url("admin/dashboard/employees/$employee->id")}}' method="POST">
							{{csrf_field()}}
							{{method_field('PUT')}}
							<input type="hidden" name="id" value="{{$employee->id}}">
							<div class="col-md-10 offset-md-1">
								<div class="row form-group ">
									<div class="col-md-3">
										<span class="label label-default">Firstname</span>
									</div>
									<div class="col-md-9{{ $errors->has('firstname') ? ' has-error' : '' }}">
										<input type="text" name="firstname" id="firstname" class="form-control" value="{{$errors->has('firstname') ? old('firstname'):  $employee->user->firstname }}">
										@if ($errors->has('firstname'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('firstname') }}</strong>
			                                </span>
			                            @endif
									</div>
								</div>

								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Lastname</span>
									</div>
									<div class="col-md-9{{ $errors->has('lastname') ? ' has-error' : '' }}">
										<input type="text" name="lastname" id="lastname" class="form-control" value="{{$errors->has('lastname') ? old('lastname'):  $employee->user->lastname }}">
										@if ($errors->has('lastname'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('lastname') }}</strong>
			                                </span>
			                            @endif
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Username</span>
									</div>
									<div class="col-md-9{{ $errors->has('username') ? ' has-error' : '' }}">
										<input type="text" name="username" id="username" class="form-control" value="{{$errors->has('username') ? old('username'):  $employee->username }}">
										@if ($errors->has('username'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('username') }}</strong>
			                                </span>
			                            @endif
									</div>
								</div>
								<div class="row">
									<input type="submit" name="" class="btn main-btn float-xs-right" value="SUBMIT">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>$('.sidebar .users').addClass('active')</script>

@endsection()