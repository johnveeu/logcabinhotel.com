@extends('layouts.dashboard')

@section('title', 'Customer - Create')

@section('contents')
	<div class="col-md-10 offset-md-2">
		<div class="row">
			<div class="col-md-12">
				<div class="card">

					<div class="card-header">
						<strong>Add New Customer</strong>
					</div>
					<div class="card-block">
						<div class="card-title text-center"><h3>ENTER CUSTOMER DETAILS</h3></div>
						<form action='{{url("admin/dashboard/customers/new")}}' method="POST">
							{{csrf_field()}}
							<div class="col-md-10 offset-md-1">
								<div class="row form-group ">
									<div class="col-md-3">
										<span class="label label-default">Firstname</span>
									</div>
									<div class="col-md-9{{ $errors->has('firstname') ? ' has-error' : '' }}">
										<input type="text" name="firstname" id="firstname" class="form-control" value="{{old('firstname')}}">
										@if ($errors->has('firstname'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('firstname') }}</strong>
			                                </span>
			                            @endif
									</div>
								</div>

								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Lastname</span>
									</div>
									<div class="col-md-9{{ $errors->has('lastname') ? ' has-error' : '' }}">
										<input type="text" name="lastname" id="lastname" class="form-control" value="{{old('lastname')}}">
										@if ($errors->has('lastname'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('lastname') }}</strong>
			                                </span>
			                            @endif
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Email Address</span>
									</div>
									<div class="col-md-9{{ $errors->has('email') ? ' has-error' : '' }}">
										<input type="text" name="email" id="email" class="form-control" value="{{old('email')}}">
										@if ($errors->has('email'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('email') }}</strong>
			                                </span>
			                            @endif
									</div>
								</div>
								<div class="row">
									<input type="submit" name="" class="btn main-btn float-xs-right" value="SUBMIT">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>$('.sidebar .users').addClass('active')</script>

@endsection()