@extends('layouts.dashboard')

@section('title', 'Reports - Dashboard')

@section('contents')
	<div class="col-md-10 offset-md-2">
		<div class="row">
			<div class="col-md-12">
				<h2 class="page-header">REPORTS</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong>GENERATE REPORTS</strong>
					</div>
					<div class="card-block">
						<div class="row">
							<div class="col-md-12">
								<ul class="nav nav-tabs nav-fill reports-nav">
									@if(Auth::user()->user->userable_type == 'App\Admin')
									<li class="nav-item">
										<a class="nav-link active" data-toggle="tab" href="#user-reports" role="tab">USER REPORTS</a>
									</li>
									@endif
									<li class="nav-item">
										<a class="nav-link"  data-toggle="tab" href="#reservation-reports" role="tab">RESERVATION REPORTS</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" data-toggle="tab" href="#room-reports" role="tab">ROOM REPORTS</a>
									</li>
								</ul>

								<div class="tab-content">
									@if(Auth::user()->user->userable_type == 'App\Admin')
										<div class="tab-pane active" id="user-reports" role="tabpanel">
											<div class="container">
												<div class="row mt-3 user-report-display">
													<div class="w-50 mw-25">
														<select class="custom-select form-control" id="user-report-select" >
															<option value="" selected>Please select a type of report</option>
															<option value="1">Master list of users</option>
															<option value="2">Users added on a date</option>
														</select>
													</div>
												</div>
												<div class="row mt-3 date-search w-50" style="display: none">
													<input type="text" name="date" id="date" class="form-control w-50 mw-50" placeholder="Select a date">
												</div>
												<div class="row mt-3">
													<div class="table-display w-100">
														
													</div>
												</div>
											</div>
										</div>
									@endif
									<div class="tab-pane" id="reservation-reports" role="tabpanel">
										<div class="container">
											<div class="row mt-3 reservation-report-display">
												<div class="w-50 mw-25">
													<select class="custom-select form-control" id="reservation-report-select" >
														<option value="" selected>Please select a type of report</option>
														<option value="1">Reservations during a month</option>
														<option value="2">Reservations done during a day</option>
														<option value="3">Reservations done during a year</option>
														<option value="4">Reservations cancelled</option>
														<option value="5">All Reservations</option>
													</select>
												</div>
											</div>
											<div class="row mt-3 date-search w-50" style="display: none">
												<input type="text" name="bla" id="date-search-input" class="form-control w-50 mw-50" placeholder="Select a date">
											</div>
											<div class="row mt-3 month-search" style="display: none">
												<div class="w-25 mw-25">
													<select class="custom-select form-control" id="month-select" >
														<option value="" selected>Month</option>
														<option value="1">January</option>
														<option value="2">February</option>
														<option value="3">March</option>
														<option value="4">April</option>
														<option value="5">May</option>
														<option value="6">June</option>
														<option value="7">July</option>
														<option value="8">August</option>
														<option value="9">September</option>
														<option value="10">October</option>
														<option value="11">November</option>
														<option value="12">December</option>
													</select>
												</div>
												<div class="ml-2">
													<input type="text" name="year" id="year" class="form-control w-50 mw-50" placeholder="Input a year">
												</div>
												<div class="mt-2 w-100">
													<button class="main-btn btn" id="month-search-btn">SEARCH</button>
												</div>
											</div>
											<div class="row mt-3 year-search" style="display: none">
												<div class="w-50 mw-25">
													<input type="text" name="year2" id="year2" class="form-control w-50 mw-50" placeholder="Year">
												</div>
												<div class="mt-2 w-100">
													<button class="main-btn btn year-search">SEARCH</button>
												</div>
											</div>
											<div class="row mt-3">
												<div class="reservation-table-display w-100">
														
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="room-reports" role="tabpanel">
										<div class="container">
											<div class="row mt-3 user-report-display">
												<div class="w-50 mw-25">
													<select class="custom-select form-control" id="room-report-select" >
														<option value="" selected>Please select a type of report</option>
														<option value="1">Room Reservations</option>
														<option value="2">Room Statistics</option>
													</select>
												</div>
											</div>
											<div class="row mt-3 room-search w-50" style="display: none">
												<select name="room_id" id="room-select" class="form-control custom-select">
													<option value="">SELECT ROOM...</option>
													@foreach($roomtypes as $roomtype)
														<optgroup label="{{$roomtype->roomtype_name}}">
															@foreach($roomtype->rooms as $room)
																<option value="{{$room->id}}">{{$room->room_name}}</option>
															@endforeach
														</optgroup>
													@endforeach
												</select>
											</div>
											<div class="row mt-3">
												<div class="room-table-display w-100">
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$('.sidebar .reports').addClass('active');

		date = flatpickr('#date-search-input', {
			altInput: true,
    		altFormat: "F j, Y",
		});

		@if(Auth::user()->user->userable_type == 'App\Admin')

		date1 = flatpickr('#date', {
			altInput: true,
    		altFormat: "F j, Y",
		});

		date1.set('onChange', function(dateStr){
			$.ajax({
				url: "{{url('getUsersCreatedAt')}}",
				method: 'POST',
				data: {date:$('#date').val()},
				beforeSend: function(){
					$('#user-report-print').prop('disabled', true);
				},
				success: function(response){
					console.log(response);
					$('.table-display').html(response).show();
					$('#user-reports-table').DataTable({
						  dom: 'Blftrip',
						  buttons: [{
						  		extend: 'print',
						  		text: 'PRINT',
						  		className: 'btn main-btn',
						  		title: '',
						  		header: false,
						  		customize: function (win){
						  			$(win.document.body)
						  				.prepend(
						  					'<div style="width: 100%; background-color: #16262e; text-align:center;"> '+
						  					'<img src="http://logcabinhotel.com/img/logo.png" style="display: block; margin: auto; padding: 10px;"/>'+
						  					'<h4 style="padding: 10px; color: #967a50; text-transform: uppercase;">'+'Users created on '+ $.format.date($('#date').val(), 'MMMM/d/YYYY')+'</h4>'+
						  					'</div>'
						  					);
						  		}
						  }]
					});
					$('#user-reports-table_length select').addClass('custom-select')

				},
				complete: function(){
					$('#user-report-print').prop('disabled', false);
				},
			})
		});
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
		$('#user-report-select').change(function(event) {
			$('.date-search').css('display', 'none');
			$('#user-report-print').prop('disabled', false);
			
			/* Act on the event */
			if(this.value == 1){
				$.ajax({
					url: "{{url('getAllUsers')}}",
					beforeSend: function(){
						$('#user-report-print').prop('disabled', true);
						$('.table-display').html('<h3 class="label mx-auto">Please wait</h3>').show();
					},
					success: function(response){
						console.log(response);
						$('.table-display').html(response).show();
						$('#user-reports-table').DataTable({
							  dom: 'Blftrip',
							  buttons: [{
							  		extend: 'print',
							  		text: 'PRINT',
							  		className: 'btn main-btn',
							  		title: '',
							  		customize: function (win){
						  			$(win.document.body)
						  				.prepend(
						  					'<div style="width: 100%; background-color: #16262e; text-align:center;"> '+
						  					'<img src="http://logcabinhotel.com/img/logo.png" style="display: block; margin: auto; padding: 10px;"/>'+
						  					'<h4 style="padding: 10px; color: #967a50; text-transform: uppercase;">'+'Master List of Users'+'</h4>'+
						  					'</div>'
						  					);
						  		}
							  }]
						});
						$('#user-reports-table_length select').addClass('custom-select')
					},
					complete: function(){
						$('#user-report-print').prop('disabled', false);
					},
				});
			}else if(this.value == 2){
				$('.date-search').css('display', 'block');
			}else{
				alert('disable button');
			}

		});
		@endif
		$('#reservation-report-select').change(function(event){
			$('.reservation-report-print').prop('disabled', true);
			if(this.value == 1){
				$('.month-search').prop('style', '');
				$('.date-search').prop('style', 'display:none');
				$('.year-search').prop('style', 'display:none');
				$('.reservation-table-display').html('').show();

			}else if(this.value ==2){
				$('.month-search').prop('style', 'display:none');
				$('.year-search').prop('style', 'display:none')
				$('.date-search').prop('style', '');
				$('.reservation-table-display').html('').show();

			}else if(this.value == 3){
				$('.month-search').prop('style', 'display:none');
				$('.date-search').prop('style', 'display:none');
				$('.year-search').prop('style', '');
				$('.reservation-table-display').html('').show();

			}else if(this.value == 4){
				$('.month-search').prop('style', 'display:none');
				$('.date-search').prop('style', 'display:none');
				$('.year-search').prop('style', 'display:none');
				$('.reservation-table-display').html('').show();

				$.ajax({
					url: "{{url('getCancelledReservations')}}",

					beforeSend: function(){
						$('.reservation-table-display').html('<h3 class="label mx-auto">Please wait</h3>').show();
						$('#reservation-report-print').prop('disabled', true);
					},
					success: function(response){
						console.log(response);
						$('.reservation-table-display').html(response).show();
						$('#reservation-table').DataTable({
							dom: 'Blftrip',
						  	buttons: [{
						  		extend: 'print',
						  		text: 'PRINT',
						  		className: 'btn main-btn',
						  		title: '',
						  		customize: function (win){
						  			$(win.document.body)
						  				.prepend(
						  					'<div style="width: 100%; background-color: #16262e; text-align:center;"> '+
						  					'<img src="http://logcabinhotel.com/img/logo.png" style="display: block; margin: auto; padding: 10px;"/>'+
						  					'<h4 style="padding: 10px; color: #967a50; text-transform: uppercase;">'+'Cancelled Reservations'+'</h4>'+
						  					'</div>'
						  					);
						  		}
						  	}]
						});
						$('#reservation-table_length select').addClass('custom-select')
					},
					complete: function(){
						$('#reservation-report-print').prop('disabled', false);
					}
				});
			}else if(this.value == 5){
				$('.month-search').prop('style', 'display:none');
				$('.date-search').prop('style', 'display:none');
				$('.year-search').prop('style', 'display:none');
				$('.reservation-table-display').html('').show();

				$.ajax({
					url: "{{url('getAllReservations')}}",

					beforeSend: function(){
						$('.reservation-table-display').html('<h3 class="label mx-auto">Please wait</h3>').show();
						$('#reservation-report-print').prop('disabled', true);
					},
					success: function(response){
						console.log(response);
						$('.reservation-table-display').html(response).show();
						$('#reservation-table').DataTable({
							dom: 'Blftrip',
							  buttons: [{
							  		extend: 'print',
							  		text: 'PRINT',
							  		className: 'btn main-btn',
							  		title: '',
							  		customize: function (win){
						  			$(win.document.body)
						  				.prepend(
						  					'<div style="width: 100%; background-color: #16262e; text-align:center;"> '+
						  					'<img src="http://logcabinhotel.com/img/logo.png" style="display: block; margin: auto; padding: 10px;"/>'+
						  					'<h4 style="padding: 10px; color: #967a50; text-transform: uppercase;">'+'Master List of Reservations'+'</h4>'+
						  					'</div>'
						  					);
						  		}
							  }]
						});
						$('#reservation-table_length select').addClass('custom-select')
					},
					complete: function(){
						$('#reservation-report-print').prop('disabled', false);
					}
				});
			}
		});

		$('#month-search-btn').click(function(){
			if($('#month-select').val() && $('#year').val()){
				console.log(true);
				$.ajax({
					url: "{{url('getReservationsOnMonth')}}",
					data: {month: $('#month-select').val(), year: $('#year').val()},
					method: 'POST',
					beforeSend: function(){
						$('.reservation-table-display').html('<h3 class="label mx-auto">Please wait</h3>').show();
						$('#reservation-report-print').prop('disabled', true);
					},
					success: function(response){
						console.log(response);
						$('.reservation-table-display').html(response).show();
						$('#reservation-table').DataTable({
							  dom: 'Blftrip',
							  buttons: [{
							  		extend: 'print',
							  		text: 'PRINT',
							  		className: 'btn main-btn',
							  		title: '',
							  		customize: function (win){
						  			$(win.document.body)
						  				.prepend(
						  					'<div style="width: 100%; background-color: #16262e; text-align:center;"> '+
						  					'<img src="http://logcabinhotel.com/img/logo.png" style="display: block; margin: auto; padding: 10px;"/>'+
						  					'<h4 style="padding: 10px; color: #967a50; text-transform: uppercase;">'+ 'Reservations during '+ $('#month-select option:selected').text() + ', '+ $('#year').val()+'</h4>'+
						  					'</div>'
						  					);
						  			}
							  }]
						});
						$('#reservation-table_length select').addClass('custom-select')
					},
					complete: function(){
						$('#reservation-report-print').prop('disabled', false);
					},
					});
			}
			return false;
		});

		$('.year-search').click(function(){
			if($('#year2').val()){
				console.log(true);
				$.ajax({
					url: "{{url('getReservationsOnYear')}}",
					data: { year: $('#year2').val()},
					method: 'POST',
					beforeSend: function(){
						$('.reservation-table-display').html('<h3 class="label mx-auto">Please wait</h3>').show();
						$('#reservation-report-print').prop('disabled', true);
					},
					success: function(response){
						console.log(response);
						$('.reservation-table-display').html(response).show();
						$('#reservation-table').DataTable({
							dom: 'Blftrip',
							  buttons: [{
							  		extend: 'print',
							  		text: 'PRINT',
							  		className: 'btn main-btn',
							  		title: '',
							  		customize: function (win){
						  			$(win.document.body)
						  				.prepend(
						  					'<div style="width: 100%; background-color: #16262e; text-align:center;"> '+
						  					'<img src="http://logcabinhotel.com/img/logo.png" style="display: block; margin: auto; padding: 10px;"/>'+
						  					'<h4 style="padding: 10px; color: #967a50; text-transform: uppercase;">'+ 'Reservations during '+ $('#year2').val()+'</h4>'+
						  					'</div>'
						  					);
						  			}
							  }]
						});
						$('#reservation-table_length select').addClass('custom-select')
					},
					complete: function(){
						$('#reservation-report-print').prop('disabled', false);
					},
					});
			}
			return false;
		});

		date.set('onChange', function(dateStr){
			$.ajax({
				url: "{{url('getReservationsOnDate')}}",
				method: 'POST',
				data: {date:$('#date-search-input').val()},
				beforeSend: function(){
					$('.reservation-table-display').html('<h3 class="label mx-auto">Please wait</h3>').show();
					$('#reservation-report-print').prop('disabled', true);
				},
				success: function(response){
					console.log(response);
					$('.reservation-table-display').html(response).show();
					$('#reservation-table').DataTable({
						dom: 'Blftrip',
						  buttons: [{
						  		extend: 'print',
						  		text: 'PRINT',
						  		className: 'btn main-btn',
						  		title: '',
						  		customize: function (win){
						  			$(win.document.body)
						  				.prepend(
						  					'<div style="width: 100%; background-color: #16262e; text-align:center;"> '+
						  					'<img src="http://logcabinhotel.com/img/logo.png" style="display: block; margin: auto; padding: 10px;"/>'+
						  					'<h4 style="padding: 10px; color: #967a50; text-transform: uppercase;">'+ 'Reservations during '+ dateStr+'</h4>'+
						  					'</div>'
						  					);
						  			}

						  }]
					});
					$('#reservation-table_length select').addClass('custom-select')
				},
				complete: function(){
					$('#reservation-report-print').prop('disabled', false);
				},
			})
		});

		$('#room-report-select').change(function(event) {
			if(this.value == 1){
				$('.room-search').prop('style', '');
			}else if(this.value == 2){
				$('.room-search').prop('style', 'display:none');
				$.ajax({
					url: "{{url('getRoomReservationStatistics')}}",
					method: 'GET',

					beforeSend: function(){
						$('.room-table-display').html('<h3 class="label mx-auto">Please wait</h3>').show();
						$('#room-report-print').prop('disabled', true);
					},
					success: function(response){
						$('.room-table-display').html(response).show();
						$('#room-report').DataTable({
							dom: 'Blftrip',
							  buttons: [{
							  		extend: 'print',
							  		text: 'PRINT',
							  		className: 'btn main-btn',
							  		title: '',
							  		customize: function (win){
						  			$(win.document.body)
						  				.prepend(
						  					'<div style="width: 100%; background-color: #16262e; text-align:center;"> '+
						  					'<img src="http://logcabinhotel.com/img/logo.png" style="display: block; margin: auto; padding: 10px;"/>'+
						  					'<h4 style="padding: 10px; color: #967a50; text-transform: uppercase;">'+ 'Reservation Statistics'+'</h4>'+
						  					'</div>'
						  					);
						  			}
							  }]});
						$('#room-report_length select').addClass('custom-select')

						},

					complete: function(){
						$('#room-report-print').prop('disabled', false);
					},
				})
			}
		});

		$('#room-select').change(function(event){
			$.ajax({
				url: "{{url('getRoomReservation')}}",
				method: 'POST',
				data: {room_id:this.value},
				beforeSend: function(){
					$('.room-table-display').html('<h3 class="label mx-auto">Please wait</h3>').show();
					$('#room-report-print').prop('disabled', true);
				},
				success: function(response){
					console.log(response);
					$('.room-table-display').html(response).show();
					$('#reservation-table').DataTable({
						dom: 'Blftrip',
						  buttons: [{
						  		extend: 'print',
						  		text: 'PRINT',
						  		className: 'btn main-btn',
						  		title: '',
						  		customize: function (win){
						  			$(win.document.body)
						  				.prepend(
						  					'<div style="width: 100%; background-color: #16262e; text-align:center;"> '+
						  					'<img src="http://logcabinhotel.com/img/logo.png" style="display: block; margin: auto; padding: 10px;"/>'+
						  					'<h4 style="padding: 10px; color: #967a50; text-transform: uppercase;">'+ 'Reservations during '+ dateStr+'</h4>'+
						  					'</div>'
						  					);
						  			}
						  }]
					});
					$('#reservation-table_length select').addClass('custom-select')
					
				},
				complete: function(){
					$('#room-report-print').prop('disabled', false);
				},
			})
		});
	</script>
@endsection