<div class="col-md-2 col-sm-1 sidebar">
    <div class="img-container">
        <img class="img-fluid" src="/img/logo.png"></img>
    </div>
    <ul class="nav menu flex-column mx-auto">
        <li class="nav-item px-auto">
            <a href='{{Auth::user()->user->userable_type == "App\Admin" ? url("admin/home") : url("employee/home")}}' class="nav-link dashboard"><i class="fa fa-tachometer"></i>Dashboard</a>
        </li>
        <li class="nav-item px-auto">
            <a class="nav-link reservations" href='{{Auth::user()->user->userable_type == "App\Admin" ? url("admin/dashboard/reservations/") : url("employee/dashboard/reservations/")}}'><i class="fa fa-calendar-check-o"></i>Reservations</a>
        </li>
        <li class="nav-item px-auto">
            <a class="nav-link charges" href='{{Auth::user()->user->userable_type == "App\Admin" ? url("admin/dashboard/charges/") : url("employee/dashboard/charges/")}}'><i class="fa fa-money"></i>Charges</a>
        </li>
        @if(Auth::user()->user->userable_type == "App\Admin")
            <li class="nav-item px-auto">
                <a class="nav-link users" href='{{url("/admin/dashboard/users")}}'><i class="fa fa-group"></i>Users</a>
            </li>
        @endif
        <li class="nav-item px-auto">
            <a class="nav-link reports" href='{{Auth::user()->user->userable_type == "App\Admin" ? url("admin/dashboard/reports/") : url("employee/dashboard/reports/")}}'><i class="fa fa-file-text-o"></i>Reports</a>
        </li>>    
    </ul>
</div>