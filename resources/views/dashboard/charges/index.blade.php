@extends('layouts.dashboard')

@section('title', 'Charges')

@section('contents')
	<div class="col-md-10 offset-md-2">
		<div class="card">
			<div class="card-header">
				<strong>CHARGES</strong>
			</div>
			<div class="card-block">
				<p>To be able to see the dashboard for charges, kindly go to <a href="www.stripe.com" class="main-color label">Stripe</a> and log-in using the details below. Thank you!</p>
				<div class="row">
					<div class="col-md-12">
							<p class="text-xs-center">
							<span class="label label-default">Username:</span> johnveeuminga@gmail.com
							
							</p>
							<p>
								<span class="label label-default d-inline-block">Password:</span> logcabinhotel
							</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>$('.sidebar .charges').addClass('active')</script>
@endsection