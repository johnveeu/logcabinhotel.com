@extends('layouts.dashboard')

@section('title' , 'Reservation - Dashboard')

@section('contents')
	@php
		$total = 0;
	@endphp
	@foreach($reservation->ammenities as $ammenity)
		@php
			$total += $ammenity->price * $ammenity->pivot->qty;
		@endphp
	@endforeach
	<div class="col-md-10 offset-md-2">
		<div class="row">
			<div class="card w-100">
				<div class="card-header">
					<strong>Edit Reservation</strong>
				</div>
				<div class="card-block">
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-danger error-alert" role="alert" style="display: none; margin-top: 10px">
										    
							</div>
						</div>
					</div>
					<form id="edit-form" action='{{Auth::user()->user->userable_type == "App\Admin" ? url("admin/dashboard/reservations/$reservation->id") : url("employee/dashboard/reservations/$reservation->id")}}' method="POST">
						{{csrf_field()}}
						{{method_field('PUT')}}
						<div class="row">
							<div class="col-md-6">
								<h4 class="card-title">Reservation Details</h4>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Firstname</span>
									</div>
									<div class="col-md-9">
										<input type="text" name="firstname" id="firstname" class="form-control" value="{{$reservation->firstname}}">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Lastname</span>
									</div>
									<div class="col-md-9">
										<input type="text" name="lastname" id="lastname" class="form-control" value="{{$reservation->lastname}}">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Check-in</span>
									</div>
									<div class="col-md-9">
										<input type="text" name="check_in" id="check-in" class="form-control" value="{{$reservation->start_date}}">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Check-out</span>
									</div>
									<div class="col-md-9">
										<input type="text" name="check_out" id="check-out" class="form-control" value="{{$reservation->end_date}}">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Cut-off date</span>
									</div>
									<div class="col-md-9">
										<input type="text" name="cutoff" id="cut-off" class="form-control" value="{{$reservation->cutoff_date}}">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Stay Length </span>
									</div>
									<div class="col-md-9">
										<input type="text" name="stay_length" class="form-control" id="stay_length" value="{{$reservation->stay_length}}" readonly>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Adults</span>
									</div>
									<div class="col-md-9">
										<input type="text" name="adults" id="adults" class="form-control" value="{{$reservation->adults}}" placeholder="adults">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Children</span>
									</div>
									<div class="col-md-9">
										<input type="text" name="children" id="children" class="form-control" value="{{$reservation->children}}">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Code </span>
									</div>
									<div class="col-md-9">
										<input type="text" name="confirmation_code" class="form-control" value="{{$reservation->confirmation_code}}" disabled>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Charge </span>
									</div>
									<div class="col-md-9">
										<input type="text" name="charge" class="form-control" id="charge" value="{{$reservation->charge->amount/100}}" readonly>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Total </span>
									</div>
									<div class="col-md-9">
										<input type="number" step="0.02" name="total" class="form-control" id="total" value="{{$reservation->amount}}" readonly>
									</div>
								</div>
							</div>
							<input type="hidden" name="room_rate" id="room_rate" value="{{$reservation->amount}}">
							<div class="col-md-6">
								<h4 class="card-title">Room Details</h4>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Room </span>
									</div>
									<div class="col-md-9">
										<select name="room_id" id="room-select" class="form-control custom-select">
											<option value="">SELECT ROOM...</option>
											@foreach($roomtypes as $roomtype)
												<optgroup label="{{$roomtype->roomtype_name}}">
													@foreach($roomtype->rooms as $room)
														<option value="{{$room->id}}" {{$reservation->room_id == $room->id ? "selected" : ""}}>{{$room->room_name}}</option>
													@endforeach
												</optgroup>
											@endforeach
										</select>
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Offseason Rate </span>
									</div>
									<div class="col-md-9">
										<input type="text" id="off_rate" disabled value="{{$reservation->room->offseason_rate}}" class="form-control">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default" >Peakseason Rate </span>
									</div>
									<div class="col-md-9">
										<input type="text" id="peak_rate" disabled value="{{$reservation->room->peakseason_rate}}" class="form-control">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Midseason Rate </span>
									</div>
									<div class="col-md-9">
										<input type="text" id="mid_rate" disabled value="{{$reservation->room->midseason_rate}}" class="form-control">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Season </span>
									</div>
									<div class="col-md-9">
										<input type="text" id="season-display" disabled value="" class="form-control">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Maximum Capacity </span>
									</div>
									<div class="col-md-9">
										<input type="text" id="capacity" disabled value="{{$reservation->room->roomtype->capacity}}" class="form-control">
									</div>
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Ammenities Total: </span>
									</div>
									<div class="col-md-9">
										<input type="number" readonly name="ammenity_total" id="ammenity_total" value = "{{$total}}">
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<a class="btn main-btn btn-block" href="#ammenities-modal" data-toggle="modal" role="button">MANAGE ADD ONS</a>
									</div>
									<div class="col-md-6">
										<button class="btn main-btn btn-block" id="check-avail">CHECK AVAILABILITY</button>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 ">
										<div class="alert alert-info avail-alert" role="alert" style="visibility: hidden; margin-top: 10px">
										    
										</div>
									</div>	
								</div>
								<div class="row form-group">
									<div class="col-md-3">
										<span class="label label-default">Ammenities: </span>
									</div>
									<div class="col-md-9 ammenity-container">
										@foreach($reservation->ammenities as $ammenity)
											<p>{{$ammenity->pivot->qty}} {{$ammenity->ammenity_desc}} (Php {{$ammenity->price}} each)</p>
										@endforeach
									</div>
								</div>
							</div>
						</div>
						
						<input type="hidden" id="season" name="season" value="3">
						<input type="hidden" id="res_id" name="res_id" value="{{$reservation->id}}">
						<div class="row">
							<div class="col-md-12">
								<input type="submit" name="" class="btn main-btn btn-block" value="SAVE" id="submit">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="ammenities-modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Ammenities</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							@foreach($ammenities as $ammenity)
							<div class="row">
								<div class="col-md-6">
									<span class="label label-default">{{$ammenity->ammenity_desc}}</span>
								</div>
								<div class="col-md-6">
									@if((($reservation->ammenities->count())==0))
										<input type="number" step = 1  name="{{$ammenity->ammenity_code}}" value="" form="edit-form" data-default="" data-price="{{$ammenity->price}}">
									@else
										@foreach($reservation->ammenities as $res_ammenity)
											@if($res_ammenity->id == $ammenity->id)
												<input type="number" step=1 max="{{$ammenity->max}}" name="{{$ammenity->ammenity_code}}" value="{{$res_ammenity->pivot->qty}}" form="edit-form" data-default={{$res_ammenity->pivot->qty}} data-price="{{$ammenity->price}}">
											@else
												<input type="number" step=1 max="{{$ammenity->max}}" name="{{$ammenity->ammenity_code}}" value="" form="edit-form" data-default="{{$res->ammenity->pivot->qty}}" data-price="{{$ammenity->price}}">
											@endif
										@endforeach
									@endif
								</div>
							</div>
						@endforeach
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn main-btn" id="edit-modal-close">RESET</button>
					<button type="button" class="btn main-btn" id="ammenity-modal-save">SAVE</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
@endsection

@section('scripts')
		<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript">
	  Stripe.setPublishableKey('pk_test_Z5f7qKe3xfrQbKHSurxlxps5');
	</script>
	<script>
		$(document).ready(function(){
			$('.sidebar .reservations').addClass('active');
			var checkIn = flatpickr('#check-in',{
				altInput: true,
		    	altFormat: "F j, Y",
			});

			var checkOut = flatpickr('#check-out',{
				altInput: true,
		    	altFormat: "F j, Y",
		    	minDate: new Date($('#check-in').val()).fp_incr(1),
			});

			var cutOff = flatpickr('#cut-off', {
				altInput: true,
				altFormat: "F j, Y",
				maxDate: new Date($('#check-in').val()).fp_incr(-1)
			});

			$.ajax({
				url: "{{url('getSeason')}}",
				method: 'POST',
				headers: {
               	 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            	},
            	data: {date: $('#check-in').val()},
            	success: function(response){
        			$('#season').val(response);
            		if(response == 1){
            			$('#season-display').val('Peakseason');
            			$('#room_rate').val($('#peak_rate').val());
            		}else if(response==3){
            			$('#season-display').val('Offseason');
            			$('#room_rate').val($('#off_rate').val());

            		}else{
            			$('#season-display').val('Midseason');
            			$('#room_rate').val($('#mid_rate').val());
            		}
            	},

            	beforeSend: function(){
            		$('#check-in').prop('disabled', true);
            		$('#check-out').prop('disabled', true);
            		$('#room-select').prop('disabled', true);
					$('#check-avail').prop('disabled', true);
					$('#submit').prop('disabled', true);

            	},

            	complete: function(){
            		$('#check-in').prop('disabled', false);
            		$('#check-out').prop('disabled', false);
            		$('#room-select').prop('disabled', false);
					$('#check-avail').prop('disabled', false);
					$('#submit').prop('disabled', false);
            	}
			});

			checkIn.set('onChange', function(dateStr){
				$.ajax({
					url: "{{url('getSeason')}}",
					method: 'POST',
					headers: {
	               	 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            	},
	            	data: {date: $('#check-in').val()},
	            	success: function(response){
	        			$('#season').val(response);
	            		if(response == 1){
	            			$('#season-display').val('Peakseason');
	            			$('#room_rate').val($('#peak_rate').val());
	            		}else if(response==3){
	            			$('#season-display').val('Offseason');
	            			$('#room_rate').val($('#off_rate').val());

	            		}else{
	            			$('#season-display').val('Midseason');
	            			$('#room_rate').val($('#mid_rate').val());
	            		}
	            	},

	            	beforeSend: function(){
	            		$('#check-in').prop('disabled', true);
	            		$('#check-out').prop('disabled', true);
	            		$('#room-select').prop('disabled', true);
						$('#check-avail').prop('disabled', true);
						$('#submit').prop('disabled', true);
	            	},

	            	complete: function(){
	            		$('#check-in').prop('disabled', false);
	            		$('#check-out').prop('disabled', false);
	            		$('#room-select').prop('disabled', false);
						$('#check-avail').prop('disabled', false);

	            		if(new Date($('#check-in').val()) >= new Date($('#check-out').val())){
								checkOut.setDate(new Date(dateStr).fp_incr(1));
						}
						checkOut.set('minDate', new Date(dateStr).fp_incr(1));
						cutOff.set('maxDate', new Date(dateStr).fp_incr(-1));
						$('#stay_length').val((((new Date($('#check-out').val())) - new Date($('#check-in').val()))/(1000*60*60*24)));
						$('#total').val(($("#stay_length").val()*$('#room_rate').val())+parseInt($('#ammenity_total').val()));
						$('#charge').val($('#total').val()*0.50);
	           		 }
				});
				
			});

			// checkOut.set('minDate', new Date($('check-in').val()).fp_incr(1));

			checkOut.set('onChange', function(dateStr){
				$('#stay_length').val((((new Date($('#check-out').val())) - new Date($('#check-in').val()))/(1000*60*60*24)));
				$('#total').val(($("#stay_length").val()*$('#room_rate').val())+parseInt($('#ammenity_total').val()));
				$('#charge').val($('#total').val()*0.50);
			});

			$('#room-select').change(function(){
				$('#check-in').prop('disabled', true);
        		$('#check-out').prop('disabled', true);
        		$('#room-select').prop('disabled', true);
				$('#check-avail').prop('disabled', true);
				$('#submit').prop('disabled', true);
				$.ajax({
					url: "{{url('changeRoomRates')}}",
					headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                	},
                	data:{id: $('#room-select').val()},
                	dataType: 'JSON',
                	success: function(response){
                		$('#peak_rate').val(response.peakseason_rate);
                		$('#off_rate').val(response.offseason_rate);
                		$('#mid_rate').val(response.midseason_rate);
                		$('#capacity').val(response.roomtype.capacity);
                	},

                	complete: function(){
                		$('#check-in').prop('disabled', false);
	            		$('#check-out').prop('disabled', false);
	            		$('#room-select').prop('disabled', false);
						$('#check-avail').prop('disabled', false);
						// $('#submit').prop('disabled', false);

						if($('#season').val()==1){
							$('#room_rate').val($('#peak_rate').val());
						}else if($('#season').val()==3){
							$('#room_rate').val($('#off_rate').val());
						}else{
							$('#room_rate').val($('#mid_rate').val());
						}
						$('#total').val(($("#stay_length").val()*$('#room_rate').val())+parseInt($('#ammenity_total').val()));
						
						$('#charge').val($('#total').val()*0.50);
                	}
				});
			});

			$('#check-avail').click(function(){
				$('#check-in').prop('disabled', true);
        		$('#check-out').prop('disabled', true);
        		$('#room-select').prop('disabled', true);
				$('#check-avail').prop('disabled', true);
				$('#submit').prop('disabled', true);
				$('#check-avail').html('PLEASE WAIT..');
				$.ajax({
					url: "{{url('checkRoomAvailability')}}",
					method: "POST",
					headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            	},
	            	data:{checkIn: $('#check-in').val(), checkOut: $('#check-out').val(), id:$('#room-select').val(), capacity: parseInt($('#adults').val()) + parseInt($('#children').val()) },
	            	success: function(response){
	            		if(response=="true"){
			      			$(".avail-alert").html('The room is available!').css('visibility', 'visible');
			      			$('#submit').prop('disabled', false);
			      		}else{
			      			$(".avail-alert").html('The room is not available!').css('visibility', 'visible');
			      			$('#submit').prop('disabled', true);
			      		}
	            	},
	            	complete: function(){
	            		$('#check-in').prop('disabled', false);
	            		$('#check-out').prop('disabled', false);
	            		$('#room-select').prop('disabled', false);
						$('#check-avail').prop('disabled', false);	
						$('#check-avail').html('CHECK AVAILABILITY');
	            	}
				});
			});

			$('#edit-form').submit(function(){
				$('.error-alert').css('display','none');
				if(parseInt($('#adults').val())+parseInt($('#children').val())>parseInt($('#capacity').val())){
					$(".error-alert").html("<strong>Oops!</strong> It seems that you have exceeded the room's capacity!").css('display', 'block');
					return false;
				}
			});

			$('#edit-modal-close').click(function(e){
				$('#ammenities-modal input').map(function(){
					$(this).val($(this).data('default'));
				});
			});

			$('#ammenity-modal-save').click(function(e){
				var ammenity_total = 0;
				$('#ammenities-modal input').map(function(){
					ammenity_total += (parseInt($(this).data('price')) * parseInt($(this).val()));
				});

				$('#ammenity_total').val(ammenity_total);
				$('#total').val(($("#stay_length").val()*$('#room_rate').val())+parseInt($('#ammenity_total').val()));
				$('#charge').val($('#total').val()*0.50);
				$('#ammenities-modal').modal('hide');
			});


		});
	</script>
@endsection