@extends('layouts.dashboard');

@section('title', 'Reservations - Dashboard')
@section('contents')
	<div class="col-md-10 offset-md-2">
		<div class="row">
			<div class="col-md-12">
				<h2 class="page-header">MANAGE RESERVATIONS</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong>RESERVATIONS</strong>
					</div>
					<div class="card-block">
						@if(session()->has('success'))
							<div class="alert alert-dismissible alert-success" role="alert" style="">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
							 	 <strong>Well done!</strong> {{session('success')}}
							</div>
						@endif
						<table class="table table-striped" id="reservation-table">
							<thead>
								<tr>
									<th>Guest</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Code</th>
									<th>Room</th>
									<th>Status</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($reservations as $reservation)
								<tr>
									<td>{{$reservation->getName()}} <br> {{$reservation->user->userable->email}}</td>
									<td>{{$reservation->formatStartDate2()}}</td>
									<td>{{$reservation->formatEndDate2()}}</td>
									<td>{{$reservation->confirmation_code}}</td>
									<td>{{$reservation->room->room_name}}</td>
									<td>{{$reservation->status == 0 ? 'Saved' : 'Cancelled' }}</td>
									<td>
										<a class="btn main-btn" href='{{Auth::user()->user->userable_type == "App\Admin" ? url("admin/dashboard/reservations/$reservation->id") : url("employee/dashboard/reservations/$reservation->id")}}' role="button"><i class="fa fa-search"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>

						<a class="btn main-btn" target="_blank" href='{{url("/reservation")}}' role="button">ADD</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$('.sidebar .reservations').addClass('active');
		$(document).ready(function(){
			$('#reservation-table').DataTable();
			$('#reservation-table_length select').addClass('custom-select');

		})
	</script>
@endsection