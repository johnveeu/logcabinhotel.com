@extends('layouts.dashboard')

@section('title', 'Reservation - Dashboard')
@section('contents')
	<div class="row">
	@php
		$total = 0;
	@endphp
	@foreach($reservation->ammenities as $ammenity)
		@php
			$total += $ammenity->price * $ammenity->pivot->qty;
		@endphp
	@endforeach
		<div class="col-md-10 offset-md-2">
			<div class="card w-100">
				<div class="card-header">
					<strong>RESERVATION DETAILS</strong>
				</div>
				<div class="card-block">
					@if(session()->has('success'))
						<div class="alert alert-dismissible alert-success" role="alert" style="">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
						 	 <strong>Well done!</strong> {{session('success')}}
						</div>
					@endif
					<div class="row">
						<div class="col-md-6">
							<img class="img-fluid" src="{{$reservation->room->roomtype->thumbnail_path}}">
							<div>
								<h3 class="text-center main-color" style="font-weight:700">{{strtoupper($reservation->room->room_name)}}</h3>
							</div>
							
						</div>
						<div class="col-md-6">
							<div class="r-cart-row">
								<p><strong class="label">Reserved By: </strong>{{$reservation->getName()}}</p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Reservation Code: </strong>{{$reservation->confirmation_code}}</p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Email Used: </strong>{{$reservation->user->userable->email}}</p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Room Type: </strong>{{$reservation->room->roomtype->roomtype_name}}</p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Check In: </strong>{{$reservation->formatDate($reservation->start_date)}}</p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Check Out: </strong>{{$reservation->formatDate($reservation->end_date)}} </p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Cut-off Date: </strong>{{$reservation->formatDate($reservation->cutoff_date)}}</p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Reservation Charge: </strong>Php {{$reservation->formatNumber($reservation->charge->amount/100)}} </p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Reservation Date: </strong>{{$reservation->formatDate($reservation->created_at)}}</p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Ammenities: </strong>
									@foreach($reservation->ammenities as $ammenity)
										{{$ammenity->pivot->qty}} {{$ammenity->ammenity_desc}} (Php {{$ammenity->price}} each). 
									@endforeach
								 </p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Ammenities total: </strong>Php {{$total}}</p>
							</div>
							<div class="r-cart-row">
								<p><strong class="label">Total: </strong>Php {{$reservation->formatNumber($reservation->amount)}} </p>
							</div>

						</div>
					</div>
				</div>
				<div class="card-footer text-center">
					<a class="btn main-btn" href="#modal-delete" data-toggle="modal" role="button">DELETE</a>
					<a class="btn main-btn" href="{{Auth::user()->user->userable_type == "App\Admin" ? url("admin/dashboard/reservations/$reservation->id/edit") : url("employee/dashboard/reservations/$reservation->id/edit")}}" role="button">EDIT</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal modal-cancel fade" id="modal-delete">
			<div class="modal-dialog modal-lg" role="document" style="max-width: 600px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" style="color: #fff">&times;</span>
							<span class="sr-only">BACK</span>
						</button>
						<h4 class="modal-title" style="color: #967a50">Delete Reservation</h4>
					</div>
					<div class="modal-body">
						<p>&nbsp; &nbsp; &nbsp;Are you sure you want to delete this record? This process is irreversible.</p>
						<form action='{{Auth::user()->user->userable_type == "App\Admin" ? url("admin/dashboard/reservations/$reservation->id") : url("employee/dashboard/reservations/$reservation->id")}}' method="POST" id="form-cancel">
							{{csrf_field()}}
							{{method_field('DELETE')}}
							<input type="hidden" name="reservation_id" value="{{$reservation->id}}">
							<input type="hidden" name="charge_code" value="{{$reservation->charge->charge_code}}">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn main-btn" data-dismiss="modal">BACK</button>
						<input class="btn main-btn" type="SUBMIT" value="PROCEEED" form="form-cancel">
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

@endsection

@section('scripts')
	<script >
		$('.sidebar .reservations').addClass('active');
	</script>
@endsection