@extends('layouts.master')

@section('title', 'Rooms')

@section('contents')
	<section class="rooms-grid-view">
		<div class="page-title parallax">
			<div class="container">
				<div class="description">
					<h2>OUR ROOMS</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum deleniti dolorem inventore ipsam laboriosam magnam qui quibusdam recusandae repudiandae tempora. A aperiam dignissimos dolorem ex fugiat harum neque, nesciunt officiis quaerat quo? Debitis earum eum itaque, modi perspiciatis tempore vel!</p>
				</div>
			</div>
		</div>

		<div class="rooms">
			<div class="content-title">
				<h2 class="page-title-center">Room Types</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum deleniti dolorem inventore ipsam laboriosam magnam qui quibusdam recusandae repudiandae tempora. A aperiam dignissimos dolorem ex fugiat harum neque, nesciunt officiis quaerat quo? Debitis earum eum itaque, modi perspiciatis tempore vel!</p>
			</div>

			<div class="room-location">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<div class="room-location-box">
								<img src="/img/rooms/hallway-1.jpg">
								<div class="caption">
									<div class="caption-title">
										Suite Rooms
									</div>
									<div class="caption-price">
										<div class="price-title">
											Starting From: 
										</div>
										<div class="price">
											P2100.00
										</div>
									</div>
									<div class="caption-desc">
										<div class="caption-desc-container">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, quia?
											<a class="main-btn btn-block" href="#" role="button">View Rooms</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="room-location-box">
								<img src="/img/rooms/hallway-1.jpg">
								<div class="caption">
									<div class="caption-title">
										Deluxe Rooms
									</div>
									<div class="caption-price">
										<div class="price-title">
											Starting From: 
										</div>
										<div class="price">
											P3000.00
										</div>
									</div>
									<div class="caption-desc">
										<div class="caption-desc-container">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, quia?
											<a class="main-btn btn-block" href="#" role="button">View Rooms</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="room-location-box">
								<img src="/img/rooms/hallway-1.jpg">
								<div class="caption">
									<div class="caption-title">
										Standard Rooms
									</div>
									<div class="caption-price">
										<div class="price-title">
											Starting From: 
										</div>
										<div class="price">
											P4200.00
										</div>
									</div>
									<div class="caption-desc">
										<div class="caption-desc-container">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, quia?
											<a class="main-btn btn-block" href="#" role="button">View Rooms</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="room-location-box">
								<img src="/img/rooms/hallway-1.jpg">
								<div class="caption">
									<div class="caption-title">
										Premier Rooms
									</div>
									<div class="caption-price">
										<div class="price-title">
											Starting From: 
										</div>
										<div class="price">
											P4200.00
										</div>
									</div>
									<div class="caption-desc">
										<div class="caption-desc-container">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, quia?
											<a class="main-btn btn-block" href="#" role="button">View Rooms</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

