<div class="room-widget">
	<h2 class="widget-title">
		Room Information
	</h2>
	<div class="r-cart-room">
		<img src="{{url($roomtype['thumbnail_path'])}}" class="img-fluid">
		<h3>{{$roomtype['roomtype_name']}}</h3>
	</div>
	<div class="r-cart-row">
		<strong>Check in: </strong>
		<span>{{$reservationDetails['checkIn']}}</span>
	</div>
	<div class="r-cart-row">
		<strong>Check Out: </strong>
		<span>{{$reservationDetails['checkOut']}}</span>
	</div>
	<div class="r-cart-row">
		<strong>Adults: </strong>
		<span>{{$reservationDetails['adults']}}</span>
	</div>
	<div class="r-cart-row">
		<strong>Child: </strong>
		<span>{{$reservationDetails['children']}}</span>
	</div>
	<div class="r-cart-total">
		<strong>Total: </strong>
		<span>{{$reservationDetails['total']}}</span>
	</div>		
</div>