<h2 class="page-title-center">Available Rooms</h2>
@if(isset($roomtypes))
	@foreach($roomtypes['roomtypes'] as $index=>$roomtype)
		<div class="room">
			<div class="row">
				<div class="col-md-5">
					<img src='{{url($roomtype['thumbnail_path'])}}' alt="" class="img-fluid">
				</div>
				<div class="col-md-7">
					<h3 class="avl-room-title">
						{{$roomtype['roomtype_name']}}
						<span> For as low as {{$roomtypes['minPrice'][$index]}}/Night</span>
					</h3>
					<p>
						{{$roomtype['roomtype_desc']}}
					</p>
					<p><strong>Available Rooms: </strong>{{$roomtypes['availableRooms'][$index]}}</p>
					<button type="button" class="btn btn-primary next-available-rooms" value={{$roomtype['id']}}>Book a room</button>
				</div>
			</div>
		</div>
	@endforeach
@endif