<table class="table table-striped w-100" id="room-report">
	<thead>
		<tr>
			<th>Rooname</th>
			<th>Rootype</th>
			<th>Number of Reservations</th>
		</tr>
	</thead>
	<tbody>
		@foreach($res as $item)
			<tr>
				<th>{{$item['room_name']}}</th>
				<td>{{$item['room_type']}}</td>
				<td>{{$item['count']}}</td>
			</tr>
		@endforeach
			
	</tbody>
</table>
