@foreach($users as $user)
	<tr>
		<td>{{$user->firstname}}</td>
		<td>{{$user->lastname}}</td>
		@if($user->userable_type == 'App\Admin')
			<td>Admin</td>
		@elseif($user->userable_type == 'App\Employee')
			<td>Employee</td>
		@else
			<td>Customer</td>
		@endif
	</tr>
@endforeach