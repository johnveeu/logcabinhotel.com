<table class="table table-striped w-100" id="user-reports-table">
	<thead>
		<tr>
			<th>Firstname</th>
			<th>Lastname</th>
			<th>Username/Email</th>
			<th>Role</th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{$user->firstname}}</td>
				<td>{{$user->lastname}}</td>
				<td>
					@if($user->userable_type=='App\Customer')
						{{$user->userable->email}}
					@else
						{{$user->userable->username}}
					@endif
				</td>
				<td>
					@if($user->userable_type=='App\Customer')
						Customer
					@elseif($user->userable_type == 'App\Admin')
						Administrator
					@else
						Employee
					@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
