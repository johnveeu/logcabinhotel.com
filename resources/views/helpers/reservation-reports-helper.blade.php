<table class="table table-striped" id="reservation-table">
	<thead>
		<tr>
			<th>Guest</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Code</th>
			<th>Room</th>
			<th>Amount Charged</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		@foreach($reservations as $reservation)
		<tr>
			<td>{{$reservation->getName()}} <br> {{$reservation->user->userable->email}}</td>
			<td>{{$reservation->formatStartDate2()}}</td>
			<td>{{$reservation->formatEndDate2()}}</td>
			<td>{{$reservation->confirmation_code}}</td>
			<td>{{$reservation->room->room_name}}</td>
			<td>{{$reservation->charge->amount/100}}</td>
			<td>{{$reservation->status == 0 ? 'Saved' : 'Cancelled' }}</td>
		</tr>
		@endforeach
	</tbody>
</table>