@extends('layouts.master')

@section('title', 'Reservation')

@section('contents')
	<section class="reservation">
		<div class="page-title fixed-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>RESERVATION</h2>
						<p>Planning for a vacation? Log Cabin is the best place to stay.<br><strong> Book now!</strong></p>
					</div>
				</div>
			</div>
		</div>

		<div class="page">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a href="#" id="reservation-date-tab" class="nav-link active">
									<span class="tab-num">1</span>
									<span class="bar"></span>
									Reservation Info
								</a>
							</li>

							<li class="nav-item">
								<a href="#avail-rooms" id="room-tab" class="nav-link">
									<span class="tab-num">2</span>
									<span class="bar"></span>
									Search & Select Room
								</a>
							</li>

							<li class="nav-item">
								<a href="#reservation-info"  id="reservation-info-tab" class="nav-link">
									<span class="tab-num">3</span>
									<span class="bar"></span>
									Personal Info
								</a>
							</li>

							<li class="nav-item">
								<a href="#payment-info" id="payment-info-tab" class="nav-link">
									<span class="tab-num">4</span>
									<span class="bar"></span>
									Comfirm & Pay!
								</a>
							</li>

							<li class="nav-item">
								<a href="#thank-you" id="thank-you-tab" class="nav-link">
									<span class="tab-num">5</span>
									Thank you!
								</a>
							</li>
						</ul>

						<div class="search-room">
							<div class="check-availability">
								<div class="ca-title-container">
									<h2 class="ca-title title">
										Search for Rooms
									</h2>
								</div>

								<form class="ca-form" method="POST" action='{{url("/get_available_rooms")}}'>
									{{csrf_field()}}
									<div class="row">
										<div class="col-md-5 offset-md-1">
											@if ($errors->has('checkIn'))
				                                <span class="help-block">
				                                    <strong>{{ $errors->first('checkIn') }}</strong>
				                                </span>
				                            @endif
											<div class="input-group{{ $errors->has('checkIn') ? ' has-error' : '' }}">
												<span class="input-group-addon">
										 			<i class="fa fa-calendar"></i>
										 		</span>
										 		<input class="form-control date" placeholder="check-in" id="check-in" name="checkIn" value="{{ old('checkIn') }}">
										 	</div>
										 	
										</div>

										<div class="col-md-5">
											@if ($errors->has('checkOut'))
				                                <span class="help-block">
				                                    <strong>{{ $errors->first('checkOut') }}</strong>
				                                </span>
				                            @endif
											<div class="input-group{{ $errors->has('checkOut') ? ' has-error' : '' }}">
												<span class="input-group-addon">
										 			<i class="fa fa-calendar"></i>
										 		</span>
										 		<input class="form-control checkout-date" placeholder="check-out" id="check-out" name="checkOut" value ="{{ old('checkOut') }}">
										 	</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-5 offset-md-1">
											@if ($errors->has('adults'))
				                                <span class="help-block">
				                                    <strong>{{ $errors->first('adults') }}</strong>
				                                </span>
				                            @endif
											<div class="input-group{{ $errors->has('adults') ? ' has-error' : '' }}">
												<span class="input-group-addon">
										 			<i class="fa fa-user"></i>
										 		</span>
										 		<input class="form-control" placeholder="number of adults" id="adults" name="adults" value="{{ old('adults') }}">
										 	</div>
										</div>

										<div class="col-md-5">
											@if ($errors->has('adults'))
				                                <span class="help-block">
				                                    <strong>&nbsp;</strong>
				                                </span>
				                            @endif
											<div class="input-group{{ $errors->has('children') ? ' has-error' : '' }}">
												<span class="input-group-addon">
										 			<i class="fa fa-child"></i>
										 		</span>
										 		<input class="form-control" placeholder="number of children" id="children" name="children" value ="{{ old('children') }}">
										 	</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-3 btn-submit-container">
											<input type="submit" class="main-btn btn-block" value="SUBMIT" id="ca-submit" role="button"></input>
										</div>
									</div>
								</form>	
								<div class="row" style="padding-right: 10px;">
									<p class="ml-auto" ><a href='{{url("ammend/")}}' style="color:white !important; font-weight: 700;"">CANCEL/MOVE A RESERVATION</a></p>
								</div>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('scripts')
<script>
	$(document).ready(function() {
		var bool = false;
		var minDate;
		$checkoutdate =  flatpickr(".checkout-date",{
	    	altInput: true,
	    	altFormat: "F j, Y",
	    	enable: [
	    	]
		});
		flatpickr('.date',{
			minDate: new Date().fp_incr(1),
			altInput: true,
	    	altFormat: "F j, Y",
			onChange: function(dateStr){
				if(!bool)
				{
					$checkoutdate.setDate(new Date(dateStr).fp_incr(1));
				}
				$checkoutdate.set('minDate', new Date(dateStr).fp_incr(1));
				if(new Date($('.date').val()) >= new Date($('#check-out').val())){
					checkOut.setDate(new Date(dateStr).fp_incr(1));
				}
				bool = true;
			}
		});


	});
</script>

<script>
	$('.navbar .reservation').addClass('active');
</script>
@endsection
