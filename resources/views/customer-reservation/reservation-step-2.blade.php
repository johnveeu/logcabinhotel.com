@extends('layouts.master')

@section('title', 'Reservation')

@section('contents')
	<section class="reservation">
		<div class="page-title fixed-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>RESERVATION</h2>
						<p>Planning for a vacation? Log Cabin is the best place to stay.<br><strong> Book now!</strong></p>
					</div>
				</div>
			</div>
		</div>

		<div class="page">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a href="#" id="reservation-date-tab" class="nav-link">
									<span class="tab-num">1</span>
									<span class="bar"></span>
									Reservation Date
								</a>
							</li>

							<li class="nav-item">
								<a href="#avail-rooms" id="room-tab" class="nav-link active">
									<span class="tab-num">2</span>
									<span class="bar"></span>
									Search & Select Room
								</a>
							</li>

							<li class="nav-item">
								<a href="#reservation-info"  id="reservation-info-tab" class="nav-link">
									<span class="tab-num">3</span>
									<span class="bar"></span>
									Personal Info
								</a>
							</li>

							<li class="nav-item">
								<a href="#payment-info" id="payment-info-tab" class="nav-link">
									<span class="tab-num">4</span>
									<span class="bar"></span>
									Comfirm & Pay!
								</a>
							</li>

							<li class="nav-item">
								<a href="#thank-you" id="thank-you-tab" class="nav-link">
									<span class="tab-num">5</span>
									Thank you!
								</a>
							</li>
						</ul>


						<div class="available-rooms">
							<h2 class="page-title-center">Available Rooms</h2>
							@if(isset(session('roomTypeArray')['roomtypes']))
								@foreach(session('roomTypeArray')['roomtypes'] as $index=>$roomtype)
									@if($roomtype->capacity>=session('reservationDetails')->capacity)
										<div class="room" >
											<div class="row">
												<div class="col-md-12">
													<h3 class="page-title-left">
														{{$roomtype['roomtype_name']}} Rooms
														<span> </span>
													</h3>
													<p>
														{{$roomtype['roomtype_desc']}}
													</p>
													
													<p><strong>Maximum capacity: </strong>{{$roomtype['capacity']}}</p>
													@foreach(session('rooms')[$index] as $room)
														<div style="margin:10px 30px">
															<hr>
															<h3><strong>{{$room->room_name}}</strong></h3>
															<p><strong>Price: </strong>Php 
																{{session('reservationDetails')->formatNumber($room->getDisplayRate(session('reservationDetails')->getSeason()))}}
															<form action='{{url("/book")}}' method="POST">
																{{csrf_field()}}
																<input type="hidden" name="room_id" value="{{$room->id}}">
																@foreach(session('ammenities') as $ammenity)
																	<input type="number" name="{{$ammenity->ammenity_code}}" max="{{$ammenity->max}}" placeholder = "{{$ammenity->ammenity_name}}">
																@endforeach
																<input class="btn main-btn " type="submit" value="BOOK THIS ROOM">
															</form>
															<hr>
														</div>
													@endforeach	
												</div>
												<p>Php 750.00 per addtional bed.</p>
											</div>
										</div>
									@endif 
								@endforeach
							@else
								<p class="text-xs-center"><h3 class="text-xs-center">Oops! It seems that there are no available rooms to satisfy your stay.</h3></p>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

<script>
	$('.navbar .reservation').addClass('active');
</script>

