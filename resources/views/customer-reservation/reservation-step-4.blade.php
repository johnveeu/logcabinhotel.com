@extends('layouts.master')

@section('title', 'Reservation')

@section('contents')
	<section class="reservation">
		<div class="page-title fixed-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>RESERVATION</h2>
						<p>Planning for a vacation? Log Cabin is the best place to stay.<br><strong> Book now!</strong></p>
					</div>
				</div>
			</div>
		</div>

		<div class="page">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a href="#" id="reservation-date-tab" class="nav-link">
									<span class="tab-num">1</span>
									<span class="bar"></span>
									Reservation Date
								</a>
							</li>

							<li class="nav-item">
								<a href="#avail-rooms" id="room-tab" class="nav-link">
									<span class="tab-num">2</span>
									<span class="bar"></span>
									Search & Select Room
								</a>
							</li>

							<li class="nav-item">
								<a href="#reservation-info"  id="reservation-info-tab" class="nav-link">
									<span class="tab-num">3</span>
									<span class="bar"></span>
									Personal Info
								</a>
							</li>

							<li class="nav-item">
								<a href="#payment-info" id="payment-info-tab" class="nav-link active">
									<span class="tab-num">4</span>
									<span class="bar"></span>
									Comfirm & Pay!
								</a>
							</li>

							<li class="nav-item">
								<a href="#thank-you" id="thank-you-tab" class="nav-link">
									<span class="tab-num">5</span>
									Thank you!
								</a>
							</li>
						</ul>

						<div id="payment-info">
							<h2 class="page-title-center">Payment Information</h2>
							<div class="row">
								<div class="col-md-7">
									<div class="information">
										<h2 class="page-title-left">Card Details</h2>
										<p><STRONG>Amount to pay: </STRONG>Php {{number_format(session('reservationDetails')->getCharge())}}</p>
										<form method="POST" id="payment-form" action='{{url("/confirmReservation")}}'>
										{{csrf_field()}}
										<span class="payment-errors"></span>
										<div class="row">
											<div class="col-md-6">
												<div class="res-info-input">
													<input type="text" name="card-number" class="form-control" placeholder="card number" data-stripe="number" size="20" value="{{ old('card-number') }}">
												</div>
											</div>
											<div class="col-md-6">
												<div class="res-info-input">
													<input type="text" name="cvc" class="form-control" placeholder="cvc" data-stripe="cvc" size="4" value="{{ old('cvc') }}">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="res-info-input">
													<input type="text" name="exp_month" class="form-control" placeholder="month of expiration" size="2" data-stripe="exp_month" value="{{ old('exp_month') }}">
												</div>
											</div>
											<div class="col-md-6">
												<div class="res-info-input">
													<input type="text" name="exp_year" class="form-control" placeholder="year of expiration" size="2" data-stripe="exp_year" value="{{ old('exp_year') }}">
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-4">
												<button type="button" class="btn main-btn btn-submit" id="submit-payment">Confirm & Pay!</button>
											</div>
										</div>
									</form>
									</div>
									
									
								</div>
								<div class="col-md-5">
										<div class="selected-room-container" id="selected-room-container">
											<div class="room-widget">
												<h2 class="widget-title">
													Room Information
												</h2>
												<div class="r-cart-room">
													<img src="{{url(session('reservationDetails')->room->roomtype->thumbnail_path)}}" class="img-fluid">
													<h3 class="text-center"><strong>{{session('reservationDetails')->room->room_name}}</strong></h3>
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Check in: </strong>
													<span class="w-25">{{session('reservationDetails')->formatStartDate()}}</span>
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Check Out: </strong>
													<span>{{session('reservationDetails')->formatEndDate()}}</span>
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Guests: </strong>
													<span class="w-25">{{session('reservationDetails')->guests}} ({{session('reservationDetails')->adults}} {{session('reservationDetails')->adults > 1 ? "adults" : "adult"}}{{isset(session('reservationDetails')->children) ? ", ".session('reservationDetails')->children: ""}}{{isset(session('reservationDetails')->children) ?  (session('reservationDetails')->children > 1 ? " children": " child") : ""}})</span>				
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Room Rate: </strong>
													<span>Php {{number_format(session('reservationDetails')->room->getDisplayRate(session('reservationDetails')->getSeason()))}}/night</span>
												</div>
												@if(isset(session('reservationDetails')->res_ammenities))
												<div class="r-cart-row">
													<strong class="w-25">Add-ons: </strong>
													@foreach(session('reservationDetails')->res_ammenities as $ammenity)
													<span class="w-25">
														{{$ammenity->qty}} {{$ammenity->ammenity_name}} (Php {{$ammenity->price}})
													</span>
													@endforeach
												</div>
												@endif
												<div class="r-cart-total">
													<strong>Total: </strong>
													<span>Php {{number_format(session('reservationDetails')->amount)}}</span>
												</div>		
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('scripts')
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript">
	  Stripe.setPublishableKey('pk_test_Z5f7qKe3xfrQbKHSurxlxps5');
	</script>
	<script>
		$(function() {
		  var $form = $('#payment-form');
		  $('#submit-payment').click(function(event) {
		  	event.preventDefault();
		    // Disable the submit button to prevent repeated clicks:
		    $form.find('.submit').attr('disabled', true);

		    // Request a token from Stripe:
		    Stripe.card.createToken($form, stripeResponseHandler);

		    // Prevent the form from being submittedkey: "value", 
		  });

		  function stripeResponseHandler(status, response) {
			  var $form = $('#payment-form');

			  if (response.error) { // Problem!

			    // Show the errors on the form:
			    $form.find('.payment-errors').text(response.error.message);
			    $form.find('.submit').attr('disabled', false); // Re-enable submission

			  } else { // Token was created!

			    // Get the token ID:
			    var token = response.id;
			    console.log(token);

			    // Insert the token ID into the form so it gets submitted to the server:
			    $form.append($('<input type="hidden" name="stripeToken">').val(token));

			    // Submit the form:
		    	$form.submit();
			}
		}
	});

	</script>

	<script>
	$('.navbar .reservation').addClass('active');
</script>

@endsection

