@extends('layouts.master')

@section('title', 'Reservation')

@section('contents')
	<section class="reservation">
		<div class="page-title fixed-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>RESERVATION</h2>
						<p>Planning for a vacation? Log Cabin is the best place to stay.<br><strong> Book now!</strong></p>
					</div>
				</div>
			</div>
		</div>

		<div class="page">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a href="#" id="reservation-date-tab" class="nav-link">
									<span class="tab-num">1</span>
									<span class="bar"></span>
									Reservation Date
								</a>
							</li>

							<li class="nav-item">
								<a href="#avail-rooms" id="room-tab" class="nav-link">
									<span class="tab-num">2</span>
									<span class="bar"></span>
									Search & Select Room
								</a>
							</li>

							<li class="nav-item">
								<a href="#reservation-info"  id="reservation-info-tab" class="nav-link active">
									<span class="tab-num">3</span>
									<span class="bar"></span>
									Personal Info
								</a>
							</li>

							<li class="nav-item">
								<a href="#payment-info" id="payment-info-tab" class="nav-link">
									<span class="tab-num">4</span>
									<span class="bar"></span>
									Comfirm & Pay!
								</a>
							</li>

							<li class="nav-item">
								<a href="#thank-you" id="thank-you-tab" class="nav-link">
									<span class="tab-num">5</span>
									Thank you!
								</a>
							</li>
						</ul>
				
					<h2 class="page-title-center mt-5">Personal Information</h2>
					<div class="reservation-information">
								<div class="row">
									
										<div class="col-md-7">
											<form id="personal-information-form"  class="information" method="POST" action='{{url("/customers")}}'>
												{{csrf_field()}}	
											<h2 class="page-title-left">Personal Details</h2>
											<div class="row">
												<div class="col-md-4">
													<div class="res-info-input{{ $errors->has('firstname') ? ' has-error' : '' }}">
														<input type="text" name="firstname" class="form-control" placeholder="first name" id="firstname" value="{{ old('firstname') }}">
														@if ($errors->has('firstname'))
							                                <span class="help-block">
							                                    <strong>{{ $errors->first('firstname') }}</strong>
							                                </span>
							                            @endif
													</div>
												</div>
												<div class="col-md-4">
													<div class="res-info-input{{ $errors->has('milddlename') ? ' has-error' : '' }}t">
														<input type="text" name="middlename" class="form-control" placeholder ="middle name" id="middlename"  value="{{ old('middlename') }}">
														@if ($errors->has('middlename'))
							                                <span class="help-block">
							                                    <strong>{{ $errors->first('middlename') }}</strong>
							                                </span>
							                            @endif
													</div>
												</div>
												<div class="col-md-4">
													<div class="res-info-input{{ $errors->has('lastname') ? ' has-error' : '' }}">
														<input type="text" name="lastname" class="form-control" placeholder="last name" id="lastname" value="{{ old('lastname') }}">
														@if ($errors->has('lastname'))
							                                <span class="help-block">
							                                    <strong>{{ $errors->first('lastname') }}</strong>
							                                </span>
							                            @endif
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													
													<div class="res-info-input{{ $errors->has('email') ? ' has-error' : '' }}">
														<input type="email" name="email" class="form-control" placeholder="email address"  id="email" value="{{ old('email') }}">
														@if ($errors->has('email'))
							                                <span class="help-block">
							                                    <strong>{{ $errors->first('email') }}</strong>
							                                </span>
							                            @endif
													</div>
												</div>
												<div class="col-md-6">

													<div class="res-info-input{{ $errors->has('contact-number') ? ' has-error' : '' }}">
														<input type="text" name="contact-number" class="form-control" placeholder="contact number" id="contact-number" value="{{ old('contact-number') }}">
														@if ($errors->has('contact-number'))
							                                <span class="help-block">
							                                    <strong>{{ $errors->first('contact-number') }}</strong>
							                                </span>
							                            @endif
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<input  class="btn main-btn btn-submit" id="ri-proceed" type="submit" value="PROCEED">
												</div>
											</div>	
											</form>
											<div class="email-block information mt-3">
												<h2 class="page-title-left">RETRIEVE YOUR INFORMATION</h2>
												<div class="alert alert-danger mt-2" role="alert" id="no-email" style="display: none">
	                        						<strong>Oops!</strong> This email has not been usedreserved before yet.
                    							</div>
                    							<p>If you have reserved before, enter your email address below and we'll fill-up the form above for you.</p>
												<div class="w-100 res-info-input">
													<input type="email" name="email_address_1" id="email1" class="form-control" placeholder="e-mail address">
												</div>
												<div>
													<button class="btn main-btn btn-submit" id="retrieve">RETRIEVE</button>
												</div>
											</div>
										</div>
									
									<div class="col-md-5">
										<div class="selected-room-container" id="selected-room-container">
											<div class="room-widget">
												<h2 class="widget-title">
													Room Information
												</h2>
												<div class="r-cart-room">
													<img src="{{url(session('reservationDetails')->room->roomtype->thumbnail_path)}}" class="img-fluid">
													<h3 class="text-center"><strong>{{session('reservationDetails')->room->room_name}}</strong></h3>
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Check in: </strong>
													<span class="w-25">{{session('reservationDetails')->formatStartDate()}}</span>
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Check Out: </strong>
													<span>{{session('reservationDetails')->formatEndDate()}}</span>
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Guests: </strong>
													<span class="w-25">{{session('reservationDetails')->guests}} ({{session('reservationDetails')->adults}} {{session('reservationDetails')->adults > 1 ? "adults" : "adult"}}{{isset(session('reservationDetails')->children) ? ", ".session('reservationDetails')->children: ""}}{{isset(session('reservationDetails')->children) ?  (session('reservationDetails')->children > 1 ? " children": " child") : ""}})</span>				
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Room Rate: </strong>
													<span>Php {{number_format(session('reservationDetails')->room->getDisplayRate(session('reservationDetails')->getSeason()))}}/night</span>
												</div>
												@if(isset(session('reservationDetails')->res_ammenities))
												<div class="r-cart-row">
													<strong class="w-25">Add-ons: </strong>
													@foreach(session('reservationDetails')->res_ammenities as $ammenity)
													<span class="w-25">
														{{$ammenity->qty}} {{$ammenity->ammenity_name}} (Php {{$ammenity->price}})
													</span>
													@endforeach
												</div>
												@endif
												<div class="r-cart-total">
													<strong>Total: </strong>
													<span>Php {{number_format(session('reservationDetails')->amount)}}</span>
												</div>		
											</div>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('scripts')
	<script>
		$('.navbar .reservation').addClass('active');

		$('#retrieve').click(function(){
			email = $('#email1').val();
			if(email==""){
				$('#no-email').html('<strong>Oops!</strong> Please enter an email address');
				$('#no-email').prop('style', '');
				return false;
			}else{
				if(!isEmail(email)){
					$('#no-email').html('<strong>Oops!</strong> Please enter a valid email address');
					$('#no-email').prop('style', '');
					return false;
				}
				 $.ajax({
	                type: 'POST',
	                url:' {{url("/findUser")}}',
	                data: {email: email},
	                dataType: 'JSON',
	                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },
	                beforeSend: function(){
	                	$('.btn-submit').prop('disabled', true);
	                },
	                success: function(response){
	            		$('#firstname').val(response.user.firstname);
	            		$('#lastname').val(response.user.lastname);
	                    $('#email').val(response.email);
	                    if(response.middlename!=""){
	                    	$('#middlename').val(response.middlename);
	                    }
	                },
	                complete: function(response){
	                	$('.btn-submit').prop('disabled', false);
	                }
	            });
			}

			function isEmail(email) {
		 		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  		return regex.test(email);
			}
		});
	</script>
@endsection