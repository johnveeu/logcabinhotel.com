@extends('layouts.master')

@section('title', 'Reservation')

@section('contents')
	<section class="reservation">
		<div class="page-title fixed-bg">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>RESERVATION</h2>
						<p>Planning for a vacation? Log Cabin is the best place to stay.<br><strong> Book now!</strong></p>
					</div>
				</div>
			</div>
		</div>

		<div class="page">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a href="#" id="reservation-date-tab" class="nav-link">
									<span class="tab-num">1</span>
									<span class="bar"></span>
									Reservation Date
								</a>
							</li>

							<li class="nav-item">
								<a href="#avail-rooms" id="room-tab" class="nav-link">
									<span class="tab-num">2</span>
									<span class="bar"></span>
									Search & Select Room
								</a>
							</li>

							<li class="nav-item">
								<a href="#reservation-info"  id="reservation-info-tab" class="nav-link">
									<span class="tab-num">3</span>
									<span class="bar"></span>
									Personal Info
								</a>
							</li>

							<li class="nav-item">
								<a href="#payment-info" id="payment-info-tab" class="nav-link">
									<span class="tab-num">4</span>
									<span class="bar"></span>
									Comfirm & Pay!
								</a>
							</li>

							<li class="nav-item">
								<a href="#thank-you" id="thank-you-tab" class="nav-link active">
									<span class="tab-num">5</span>
									Thank you!
								</a>
							</li>
						</ul>

						<div class="congratulations">
							<div class="row ">
								<div class="col-md-12 congratulations-container">
									<h2 class="page-title-center">Congratulations! 
									</h2>
									<p class="text-center">Your room has been successfully reserved!</p>
									<p><h3 class="text-center" style="color: rgba(0,0,0,0.7)">CONFIRMATION CODE</h3></p>
									<p><h3 class="text-center conf-code">{{session('reservationDetails')->confirmation_code}}</h3></p>
									<p class="text-center">Your reservation voucher has been emailed to you. Please obtain of a copy of it and present it to the front-desk upon checking-in.</p>
									
									<div class="row">
										<div class="reservation-details-ty col-md-8 offset-md-2">
											<div class="selected-room-container" id="selected-room-container">
											<div class="room-widget">
												<h2 class="widget-title">
													Room Information
												</h2>
												<div class="r-cart-room">
													<img src="{{url(session('reservationDetails')->room->roomtype->thumbnail_path)}}" class="img-fluid">
													<h3 class="text-center"><strong>{{session('reservationDetails')->room->room_name}}</strong></h3>
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Check in: </strong>
													<span class="w-25">{{session('reservationDetails')->formatStartDate()}}</span>
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Check Out: </strong>
													<span>{{session('reservationDetails')->formatEndDate()}}</span>
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Guests: </strong>
													<span class="w-25">{{session('reservationDetails')->guests}} ({{session('reservationDetails')->adults}} {{session('reservationDetails')->adults > 1 ? "adults" : "adult"}}{{isset(session('reservationDetails')->children) ? ", ".session('reservationDetails')->children: ""}}{{isset(session('reservationDetails')->children) ?  (session('reservationDetails')->children > 1 ? " children": " child") : ""}})</span>				
												</div>
												<div class="r-cart-row">
													<strong class="w-25">Room Rate: </strong>
													<span>Php {{number_format(session('reservationDetails')->room->getDisplayRate(session('reservationDetails')->getSeason()))}}/night</span>
												</div>
												@if(isset(session('reservationDetails')->res_ammenities))
												<div class="r-cart-row">
													<strong class="w-25">Add-ons: </strong>
													@foreach(session('reservationDetails')->res_ammenities as $ammenity)
													<span class="w-25">
														{{$ammenity->qty}} {{$ammenity->ammenity_name}} (Php {{$ammenity->price}})
													</span>
													@endforeach
												</div>
												@endif
												<div class="r-cart-total">
													<strong>Total: </strong>
													<span>Php {{number_format(session('reservationDetails')->amount)}}</span>
												</div>		
											</div>
										</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('scripts')
	<script>
		$('.navbar .reservation').addClass('active');
	</script>
@endsection

