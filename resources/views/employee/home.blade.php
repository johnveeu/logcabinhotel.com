@extends('layouts.dashboard')
@section('title', 'Dashboard')
@section('contents')
    
    <div class="col-md-10 offset-md-2">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-header">Welcome, {{Auth::user()->user->firstname}}!</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 mt-3">
                <div class="card no-padding">
                    <div class="row mx-0">
                        <div class="col-md-3 widget-left ">
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="col-md-9 widget-right">
                            <div class="numb-user large">0</div>Users Today
                        </div>
                    </div>
                </div>
            </div>

             <div class="col-md-4 mt-3">
                <div class="card no-padding">
                    <div class="row mx-0">
                        <div class="col-md-3 widget-left">
                            <i class="fa fa-bed"></i>
                        </div>
                        <div class="col-md-9 widget-right">
                             <div class="numb-reservations large">0</div>Reservations Today
                        </div>
                    </div>
                </div>
            </div>

             <div class="col-md-4 mt-3">
                <div class="card">
                    <div class="row mx-0">
                        <div class="col-md-3 widget-left">
                            <i class="fa fa-money"></i>
                        </div>
                        <div class="col-md-9 widget-right">
                            <div class="total-charges large">Php 0</div>Sales Made Today
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
             <div class="col-md-4 mt-3">
                <div class="card">
                    <div class="row mx-0">
                        <div class="col-md-3 widget-left">
                            <i class="fa fa-calendar-check-o"></i>
                        </div>
                        <div class="col-md-9 widget-right">
                            <div class="arrival large">0</div> Arrivals Today
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mt-3">
                <div class="card">
                    <div class="row mx-0">
                        <div class="col-md-3 widget-left">
                            <i class="fa fa-calendar-times-o"></i>
                        </div>
                        <div class="col-md-9 widget-right">
                             <div class="departures large">0</div> Departures Today
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="col-md-4 mt-3">
                <div class="card">
                    <div class="row mx-0">
                        <div class="col-md-3 widget-left">
                            <i class="fa fa-bed"></i>
                        </div>
                        <div class="col-md-9 widget-right">
                             <div class="avail-rooms large">0</div> Available Rooms Today
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card tasks">
                    <div class="card-header">
                        <strong>Newest Reservations</strong>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Guest</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Email Used</th>
                                            <th>Code</th>
                                            <th>Room</th>
                                        </tr>
                                    </thead>
                                    <tbody class="recent-reservations">
                                    </tbody>
                                </table>
                            </div>
                        
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card tasks">
                    <div class="card-header">
                        <strong>Arrivals Today</strong>
                    </div>
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Guest</th>
                                    <th>Room</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody class="arrivals-today">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card tasks">
                    <div class="card-header">
                        <strong>Departures Today</strong>
                    </div>
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Guest</th>
                                    <th>Room</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody class="departures-today">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card tasks">
                    <div class="card-header">
                        <strong>Newest Users</strong>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Firstname</th>
                                            <th>Lastname</th>
                                            <th>Role</th>
                                        </tr>
                                    </thead>
                                    <tbody class="new-users">
                                            
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>  
    </div>

  </section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $.ajax({
                type: 'GET',
                url:' {{url("/getNewUsers")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.numb-user').html(response).show();
                }
            });

            $.ajax({
                type: 'GET',
                url:' {{url("/getNewReservations")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.numb-reservations').html(response).show();
                }
            });

            $.ajax({
                type: 'GET',
                url:' {{url("/getTotalCharges")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.total-charges').html("Php " + response).show();
                }
            });

            $.ajax({
                type: 'GET',
                url:' {{url("/getRecentUsers/3")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.new-users').html(response).show();
                }
            });

            $.ajax({
                type: 'GET',
                url: '{{url("getRecentReservations")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.recent-reservations').html(response).show();
                }
            })

            $.ajax({
                type: 'GET',
                url: '{{url("getTodayArrivals")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.arrival').html(response.length).show();
                }
            });

            $.ajax({
                type: 'GET',
                url: '{{url("getAvailableRoomsToday")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.avail-rooms').html(response).show();
                }
            });


            $.ajax({
                type: 'GET',
                url: '{{url("getTodayArrivalsTable")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.arrivals-today').html(response).show();
                }
            });


            $.ajax({
                type: 'GET',
                url: '{{url("getTodayDepartures")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.departures').html(response.length).show();
                }
            });

            $.ajax({
                type: 'GET',
                url: '{{url("getTodayDeparturesTable")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    $('.departures-today').html(response).show();
                }
            });

            $('.dashboard').addClass('active');

        });
    </script>
@endsection