@extends('layouts.master')

@section('title', 'Amend')
@section('contents')
	@php
		$total = 0;
	@endphp
	@foreach($reservation->ammenities as $ammenity)
		@php
			$total += $ammenity->price * $ammenity->pivot->qty;
		@endphp
	@endforeach
	<section class="ammend-reservation parallax" style="min-height: 100%; padding-top: 100px;">
		<div class="container">
			<div class="col-md-12">
				<div class="row">
					<div class="card w-100">
						<div class="card-header"> <h4>RESERVATION DETAILS</h4></div>
						<div class="card-block">
							@if($reservation->status == 1)
								<div class="alert alert-info" role="alert">
									<strong>This reservation has already been cancelled.</strong> You cannot amend this reservation anymore.
								</div>
							@endif

							@if(isset($edit_success))
								<div class="alert alert-dismissible alert-success" role="alert" style="">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
								 	 <strong>Well done!</strong> {{$edit_success}}
								</div>
							@endif

							@if($reservation->status == 1)
								<div class="alert alert-info" role="alert">
									<strong>This reservation has already been cancelled.</strong> You cannot ammend this reservation anymore.
								</div>
							@endif
							<h4 class="card-title main-color">{{$reservation->firstname}} {{$reservation->middlename}} {{$reservation->lastname}}</h4>
							<div class="row">
								<div class="col-md-6">
									<img class="img-fluid" src="{{$reservation->room->roomtype->thumbnail_path}}">
									<div>
										<h3 class="text-center" style="font-weight:700">{{$reservation->room->room_name}}</h3>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="r-cart-row">
										<p><strong>Reservation Code: </strong>{{$reservation->confirmation_code}}</p>
									</div>
									<div class="r-cart-row">
										<p><strong>Email Used: </strong>{{$reservation->user->userable->email}}</p>
									</div>
									<div class="r-cart-row">
										<p><strong>Room Type: </strong>{{$reservation->room->roomtype->roomtype_name}}</p>
									</div>
									<div class="r-cart-row">
										<p><strong>Check In: </strong>{{$reservation->formatDate($reservation->start_date)}}</p>
									</div>
									<div class="r-cart-row">
										<p><strong>Check Out: </strong>{{$reservation->formatDate($reservation->end_date)}} </p>
									</div>
									<div class="r-cart-row">
										<p><strong>Cut-off Date: </strong>{{$reservation->formatDate($reservation->cutoff_date)}}</p>
									</div>
									<div class="r-cart-row">
										<p><strong>Reservation Charge: </strong>Php {{$reservation->formatNumber($reservation->charge->amount/100)}} </p>
									</div>
									<div class="r-cart-row">
										<p><strong>Reservation Date: </strong>{{$reservation->formatDate($reservation->created_at)}}</p>
									</div>
									<div class="r-cart-row">
										<p><strong class="label">Reservation Date: </strong>{{$reservation->formatDate($reservation->created_at)}}</p>
									</div>
									<div class="r-cart-row">
										<p><strong class="label">Ammenities: </strong>
											@foreach($reservation->ammenities as $ammenity)
												{{$ammenity->pivot->qty}} {{$ammenity->ammenity_desc}} (Php {{$ammenity->price}} each). 
											@endforeach
										 </p>
									</div>
									<div class="r-cart-row">
										<p><strong class="label">Ammenities total: </strong>Php {{$total}}</p>
									</div>
										<div class="r-cart-row">
											<p><strong>Total: </strong>Php {{$reservation->formatNumber($reservation->amount)}} </p>
										</div>
									</div>
									</div>

						@if($reservation->status != 1)
							<div class="card-footer text-center">
								<a class="btn main-btn" href="#modal-delete" data-toggle="modal" role="button">CANCEL</a>
								@if(Carbon\Carbon::parse($reservation->cutoff_date)->gt(Carbon\Carbon::now()))
								<a class="btn main-btn" href="#modal-move" data-toggle="modal" role="button">MOVE</a>
								@endif
							</div>
						@endif
						
					</div>
				</div>
				
			</div>
		</div>
		

		<div class="modal modal-cancel fade" id="modal-delete">
			<div class="modal-dialog modal-lg" role="document" style="max-width: 600px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" style="color: #fff">&times;</span>
							<span class="sr-only">BACK</span>
						</button>
						<h4 class="modal-title" style="color: #967a50">Cancel Reservation</h4>
					</div>
					<div class="modal-body">
						<p><h4>Hotel Policy: </h4></p>
						<p>&nbsp; &nbsp; &nbsp; &nbsp; Any cancellation done 7 days before your check-in date will refund your reservation fee (50% of your reservation total). Cancellations done after the given period will result to no refunds. </p>
						<p>&nbsp; &nbsp; &nbsp; &nbsp; Moving a reservation is only allowed  7 days before your check-in date. You are not allowed to extend your stay length. You also are not allowed to change your room. If you want to change a room or add an ammenity, do contact us.</p>
						<form action='{{url("/ammend/cancel")}}' method="POST" id="form-cancel">
							{{csrf_field()}}
							<input type="hidden" name="reservation_id" value="{{$reservation->id}}">
							<input type="hidden" name="charge_code" value="{{$reservation->charge->charge_code}}">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn main-btn" data-dismiss="modal">BACK</button>
						<input class="btn main-btn" type="SUBMIT" value="CANCEL" form="form-cancel">
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<div class="modal modal-move modal-cancel fade" id="modal-move">
			<div class="modal-dialog modal-lg" role="document" style="max-width: 600px;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" style="color: #fff">&times;</span>
							<span class="sr-only">BACK</span>
						</button>
						<h4 class="modal-title" style="color: #967a50">Move Reservation</h4>
					</div>
					<div class="modal-body">
					<form action='{{url("/ammend/move")}}' method="POST" id="form-cancel">
						<div class="row">
							<div class="col-md-12">
								<label><strong>Check-In</strong></label>
								<div class="input-group">
									<span class="input-group-addon">
							 			<i class="fa fa-calendar"></i>
							 		</span>
							 		<input class="form-control checkin-date" placeholder="check-in" id="check-in" name="checkIn" value ="{{$reservation->start_date}}">
							 	</div>
							</div>	
						</div>
						<div class="row">
							<div class="col-md-12">
								<label><strong>Check-Out</strong></label>
								<div class="input-group">
									<span class="input-group-addon" style="background-color: #eceeef;">
							 			<i class="fa fa-calendar"></i>
							 		</span>
							 		<input class="form-control disabled checkout-date" placeholder="check-out" id="check-out" name="checkOut" value ="{{$reservation->end_date}}" style="background-color: gray !important;">
							 	</div>
							</div>	
						</div>
						<div class="row">
							<div class="col-md-12 ">
								<div class="alert alert-info" role="alert" >
								    
								</div>
							</div>	
						</div>
					
						<div class="btn-container">
							<button class="text-xs-center btn main-btn" id="c-avail" type="button">Check Availability</button>
							<button class="text-xs-center btn main-btn" id="move" disabled>Move</button>
						</div>
						
						<p><h4>Hotel Policy: </h4></p>
						<p>&nbsp; &nbsp; &nbsp; &nbsp; Any cancellation done 7 days before your check-in date will refund your reservation fee (50% of your reservation total). Cancellations done after the given period will result to no refunds. </p>
						<p>&nbsp; &nbsp; &nbsp; &nbsp; Moving a reservation is only allowed  7 days before your check-in date. You are not allowed to extend your stay length. You also are not allowed to change your room. If you want to change a room or add an ammenity, do contact us.</p>	
							{{csrf_field()}}
							<input type="hidden" name="reservation_id" value="{{$reservation->id}}" id="res_id">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn main-btn" data-dismiss="modal">BACK</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</section>


@endsection

@section('scripts')
	<script>
		
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip(); 
		    $checkoutdate =  flatpickr(".checkout-date",{
		    	altInput: true,
		    	altFormat: "F j, Y",
		    	enable: [
		    	]
		    });
		    $checkoutdate.set('onOpen', function(){
    			$checkoutdate.close();
    		});
    		$('.input').prop('disabled', 'true');
    		$('.input').css('background-color', '#eceeef');
		    var checkindate = flatpickr(".checkin-date",{
		    	minDate: new Date(),
		    	altInput: true,
		    	altFormat: "F j, Y",
		    	onChange: function(dateStr){
		    		$checkoutdate.setDate(new Date(dateStr).fp_incr({{$reservation->stay_length}}));
		    		$checkoutdate.set('enable', [new Date(dateStr).fp_incr({{$reservation->stay_length}})]);
		    	}
		    });

		    $('.modal-move .alert').hide();
			$('#c-avail').on('click', function(){
				console.log('blabla');
				$('#c-avail').prop('disabled', true);
				$('#c-avail').html('Checking..');
				$.ajax({
			        type: 'POST',
			        url: '/ammend/move/checkdates', 
			        data: {
			        	id: $('#res_id').val(),
			        	checkIn: $('#check-in').val(),
			        	checkOut: $('#check-out').val()
			        },
			        headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}, 
			      	success: function(response){
			      		if(response=="true"){
			      			$(".modal-move .alert").html('The room is available at these dates!').show();
			      			$('#move').prop('disabled', false);
			      		}else{
			      			$(".modal-move .alert").html('The room is not available at these dates!').show();
			      			$('#move').prop('disabled', true);
			      		}
					},	
					complete: function(){
						$('#c-avail').prop('disabled', false);
						$('#c-avail').html('Check Availability');
					}
				});
			});
		});
	</script>
	<script>
		$('.navbar .reservation').addClass('active');
	</script>
@endsection