@extends('layouts.master')

@section('title', 'Amend')

@section('contents')
	<section class="check-reservation parallax">
		<div class="container wrap">
			<div class="row">
				<div class="col-md-8 offset-md-2 page">
					<div class="card">
						<h3 class="card-header text-xs-center">Amend A Reservation</h3>
						<div class="card-block">
							@if(session()->has('message'))
								<div class="alert alert-dismissible alert-success" role="alert" style="">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
								 	 <strong>Well done!</strong> {{session('message')}}
								</div>
							@endif
							@if(session()->has('error'))
								<div class="alert alert-dismissible alert-danger" role="alert" style="">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
								  	<strong>Oops!</strong> {{session('error')}}
								</div>
							@endif
							<form action='{{url("/ammend/check")}}' method="POST">
								{{csrf_field()}}
								<div class="form-group row">
									<div class="col-sm-12">
										<input type="email" class="form-control" placeholder="email" name="email">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-12">
										<input type="text" class="form-control" name="conf_code" placeholder="confirmation code">
									</div>
								</div>
								
								<div class="form-group row">
									<div class="col-sm-12">
										<button type="submit" class="main-btn btn-submit float-right">SEARCH</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('scripts')
	<script >$('.navbar .reservation').addClass('active');</script>
@endsection