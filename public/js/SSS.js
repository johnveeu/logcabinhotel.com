$(document).ready(function() {
	var minDate;
	flatpickr('.date',{
		minDate: new Date(),
		onChange: function(){
			console.log(parseDate($(this).val()));
		}
	});


	$('.reservation #search-room-form input').change(function() {
		var stillNull = false;
		console.log('changed');
		$(".reservation #search-room-form input").each(function() {
		    if($(this).val().length===0){
		    	console.log('still null');
		    	stillNull = true;
		    }
		});

		if(!stillNull){
			console.log(stillNull);
			$(".reservation #search-room-form #bn-form-submit").removeClass('disabled').removeAttr('disabled');
		}

	});

	$('#ca-submit').click(function(e){
		var checkIn = $('.reservation #check-in').val();
		var checkOut = $('.reservation #check-out').val();
		var children = $('.reservation #children').val();
		var adults = $('.reservation #adults').val();
		$.ajaxSetup({
			headers: {
			    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
	        type: 'POST',
	        url: '/getavailablerooms',
	        data: {checkIn: checkIn, checkOut: checkOut, children: children, adults: adults}, 
	      	success: function(response){
	      		$('#available-rooms').html(response).show();
	      		$('html, body').animate({
				    scrollTop: $("#available-rooms").offset().top
				}, 500);
	      	}
		});
	});


	$(document).on('click', '.next-step', function (e) {
        var active = $('.reservation .nav-tabs .nav-item .nav-link.active');
        var next = $($('.reservation .nav-tabs .nav-item .nav-link.active').next('.nav-link'));
        console.log(next);
        console.log(active);
        // $active.next().removeClass('disabled');
        console.log(nextTab(active));

    });	
	

});

// function nextTab(elem) {
//     return ($(elem).next().find('.reservation .nav-tabs .nav-item + .nav-link[data-toggle="tab"]:not(.active)'));
// }


