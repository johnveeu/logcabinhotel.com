<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.home');
})->name('home');

Route::group(['prefix' => 'dashboard'], function(){
  Route::resource('/reservations', 'ReservationDashboardController');
  Route::resource('/charges', 'ChargesDashboardController');
  Route::resource('/users', 'UserDashboardController');
  Route::resource('/customers', 'CustomerController');
  Route::POST('/customers/new', 'CustomerController@storeNew');
  Route::resource('/admins','AdminController');
  Route::resource('/employees','EmployeeController');
  Route::resource('/reports', 'ReportsDashboardController');
});


