<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
// 

Auth::routes();

Route::get('/home', 'HomeController@home');

Route::get('/about-us', function(){
	return view('about-us');
});

Route::get('/faqs', function(){
	return view('faqs');
});

Route::get('/', function (){
	return view('index');
});

Route::get('/rooms', function (){
	return view('rooms.index');
});

// Route::get('/dashboard/reservations/', function (){
// //   return view('')
// // })

Route::get('/reservation/', 'ReservationController@index');
Route::get('/reservation/step/{step}', 'ReservationController@getReservationStep')->where(['step' => '[1-5]']);
Route::get('/ammend/', 'ReservationController@showCheckForm');
Route::post('/ammend/check', 'ReservationController@verifyInfo');
Route::post('/ammend/cancel', 'ReservationController@cancelReservation');
Route::post('/ammend/move/checkdates', 'ReservationController@checkDatesAvailability');
Route::post('/ammend/move', 'ReservationController@update');
Route::get('/email/send', 'RoomReservationController@sendEmail');

Route::get('/welcome', 'HomeController@welcome');
Route::POST('/get_available_rooms', 'RoomReservationController@getAvailableRooms');
Route::POST('/book', 'RoomReservationController@book');
Route::POST('/payreservation', 'RoomReservationController@payReservation');

Route::POST('/confirmReservation', 'RoomReservationController@confirmReservation');

Route::resource('/customers', 'CustomerController');


Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout');
  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'employee'], function () {
  Route::get('/login', 'EmployeeAuth\LoginController@showLoginForm');
  Route::post('/login', 'EmployeeAuth\LoginController@login');
  Route::post('/logout', 'EmployeeAuth\LoginController@logout');

  Route::get('/register', 'EmployeeAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'EmployeeAuth\RegisterController@register');

  Route::post('/password/email', 'EmployeeAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'EmployeeAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'EmployeeAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'EmployeeAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'customer'], function () {
  Route::get('/login', 'CustomerAuth\LoginController@showLoginForm');
  Route::post('/login', 'CustomerAuth\LoginController@login');
  Route::post('/logout', 'CustomerAuth\LoginController@logout');

  Route::get('/register', 'CustomerAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'CustomerAuth\RegisterController@register');

  Route::post('/password/email', 'CustomerAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'CustomerAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'CustomerAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'CustomerAuth\ResetPasswordController@showResetForm');
});


Route::get('getNewUsers', 'UserController@getNewUsers');
Route::get('getNewReservations', 'ReservationController@getNewReservations');
Route::get('getAvailableRoomsToday', 'ReservationController@getAvailableRoomsToday');
Route::get('getTotalCharges', 'ReservationController@getTotalCharges');
Route::get('getRecentUsers/{items}', 'UserController@indexOrdered');
Route::get('getRecentReservations', 'ReservationController@recentReservations');
Route::get('getTodayArrivals', 'ReservationController@todayArrivals');
Route::get('getTodayArrivalsTable', 'ReservationController@todayArrivalsTable');
Route::get('getTodayDepartures', 'ReservationController@todayDepartures');
Route::get('getTodayDeparturesTable', 'ReservationController@todayDeparturesTable');
Route::get('changeRoomRates', 'ReservationDashboardController@changeRoomRates');
Route::get('getAllUsers', 'UserController@getAllUsers');
Route::POST('getSeason', 'ReservationDashboardController@getSeason');
Route::POST('getUsersCreatedAt', 'UserController@getUsersCreatedAt');
Route::get('getAllReservations', 'ReservationController@getAllReservations');
Route::get('getCancelledReservations', 'ReservationController@getCancelledReservations');
Route::POST('findUser', 'CustomerController@findUser');
Route::get('getRoomReservationStatistics', 'ReservationController@getRoomReservationStatistics');
Route::POST('getReservationsOnMonth', 'ReservationController@getReservationsOnMonth');
Route::POST('getReservationsOnYear', 'ReservationController@getReservationsOnYear');
Route::POST('getReservationsOnDate', 'ReservationController@getReservationsOnDate');
Route::POST('getRoomReservation', 'ReservationController@getRoomReservation');

Route::POST('checkRoomAvailability', 'ReservationDashboardController@checkRoomAvailability');


