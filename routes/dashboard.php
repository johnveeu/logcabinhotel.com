<?php

Route::group(['prefix' => 'dashboard'], function(){
  Route::resource('/reservations/', 'ReservationDashboardController');
});

