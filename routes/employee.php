<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('employee')->user();

    //dd($users);

    return view('employee.home');
})->name('home');


Route::group(['prefix' => 'dashboard'], function(){
  Route::resource('/reservations', 'ReservationDashboardController');
  Route::resource('/charges', 'ChargesDashboardController');
  Route::resource('/reports', 'ReportsDashboardController');
});


