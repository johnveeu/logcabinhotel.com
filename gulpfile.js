const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

var paths = {
	'flatpickr': 'node_modules/flatpickr/'
}
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// elixir(mix => {
// 	mix.copy(paths.flatpickr + 'dist/flatpickr.min.css', 'resources/assets/css')
// 		.copy(paths.flatpickr + 'dist/flatpickr.js', 'resources/assets/js');

//     mix.sass('bootstrap.scss')
//     	.styles('flatpickr.min.css')
//     	.scripts('bootstrap.js')
//     	.scripts('jquery.js')
//     	.scripts('flatpickr.js')
//     	.webpack('app.js')
// })


elixir(mix => {
	// mix.styles(['index.css'], 'public/css/main.css')
        mix.scripts('main.js');
})


