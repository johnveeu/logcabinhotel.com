### Log Cabin Hotel

### Installation

• pull source
```bash
git clone https://johnveeuminga@bitbucket.org/johnveeu/logcabinhotel.com.git
```

• navigate to logcabinhotel.com
```bash
cd logcabinhotel.com
```

• run composer install
```bash
composer install
```

• migrate database and seed
```bash
php artisan migrate:refresh --seed
```

• run php artisan serve
```bash
php artisan serve
```